import torch
from layers.box_utils import jaccard, xywh_to_xyxy, encode
import numpy as np

HARDCODED_SMALL_BOX_REL_AREA = 0.00071070

MAGIC_THRESHOLD = 1.2


DEFAULT_MATCHER_OPTIONS = {
            'fg_iou_thresh': (0.3, lambda x: x >= 0),
            # threshold for determining what is a good gt match
            'hard_bg_max_iou': (0.3, lambda x: x >= 0),
            # boost similarity threshold for hw towards shrinking
            'hw_similarity_prefer_smaller_gt': (True, lambda x: isinstance(x, bool)),
            # box similarity outlier detection - within the chosen encoding
            'hw_similarity_thresh': (1.5, lambda x: isinstance(x, (int, float)) and x >= 0),
            # box similarity outlier detection - within the chosen encoding
            'xy_similarity_thresh': (0, lambda x: isinstance(x, (int, float)) and x >= 0),
            # if set, no more than max_num_... will be used for training"
            'max_num_fg_box_per_image': (300, lambda x: x is None or (isinstance(x, int) and x > 0)),
            # fixes the ratio between all bg box and number of fg-matched boxes
            'bg_to_fg_box_ratio': (2, lambda x: x >= 0),
            # percentage of "hard bg" examples to take (hard = actually matched with gt_box but below threshold)
            'hard_bg_mix': (0.3, lambda x: x >= 0),
            # percentage of "false_positive" examples to take (easy bg candidates with atypically large objecntess)
            'false_pos_mix': (0.5, lambda x: x >= 0),
            # pick hardest background "false_positive" samples (lowest scores for background class) rather than easy
            'pick_hard_false_pos': (False, lambda x: isinstance(x, bool)),
            # specifically ignore false_positives
            'ignore_false_pos': (False, lambda x: isinstance(x, bool)),
            # how many positives to drop
            'ignore_false_pos_conf_thresh': (0.5, lambda x: isinstance(x, (int, float)) and x >= 0),
            # use pick 'hard examples' for background "false_positive" samples
            'hard_example_mining': (True, lambda x: isinstance(x, bool)),
            # map all the bg examples so their gt label is this
            'background_class_index': (0, lambda x: isinstance(x, int)),
            # maximum iou by which ignore ground-truth can be matched with an easy bg
            'easy_bg_max_iou_with_ignore': (0, lambda x: x >= 0),
            # easy bg will take largest possible boxes, otherwise - random.
            'prefer_largest': (False, lambda x: isinstance(x, bool)),
        }


def default_matcher_options():
    return DEFAULT_MATCHER_OPTIONS.copy()


class DragonMatcher(object):

    def __init__(self, **kwargs) -> None:
        super().__init__()
        # setup input parameters (validation via lambda) (CURRENTLY NOT USED)
        # input_flags = default_matcher_options()
        # options = options if options else {}
        # flag_values = {k: options.get(k, default) for k, (default, _) in input_flags.items()}
        # invalid = [k for k, v in flag_values.items() if not input_flags[k][1](v)]
        # if invalid:
        #     raise ValueError('Invalid values for input flags %s' % ', '.join(invalid))
        #
        # for k, v in flag_values.items():
        #     setattr(self, '_%s' % k, v)
        options = dict() if kwargs is None else kwargs

        self._min_iou_for_small_box_match = options.get('min_iou_for_small_box_match', 0.2)

        # dynamically found during run

        self._small_box_rel_area = None
        self._small_prior_indexes = None

        self._match_store = None
        self._match_label2id = dict(match=1337, low_thresh=666)

    def __call__(self, threshold, gt_rel_boxes, priors, variances, labels):
        """Match each prior box with the ground truth box of the highest jaccard
        overlap, encode the bounding boxes, then return the matched indices
        corresponding to both confidence and location preds.
        Args:
            threshold: (float) The overlap threshold used when mathing boxes.
            gt_rel_boxes: (tensor) Ground truth boxes, Shape: [num_obj, num_priors].
            priors: (tensor) Prior boxes from priorbox layers, Shape: [n_priors,4].
            variances: (tensor) Variances corresponding to each prior coord,
                Shape: [num_priors, 4].
            labels: (tensor) All the class labels for the image, Shape: [num_obj].
        Return:
            The matched indices corresponding to 1)location and 2)confidence preds.
        """

        # clear stored matches
        self._match_store = {}

        # jaccard index
        # AJB - note that we convert the priors to xyxy each time?!
        if self._small_box_rel_area is None:
            self._small_box_rel_area, self._small_prior_indexes = DragonMatcher.estimate_small_box_threshold(priors)

        # support no ground truth
        if not len(gt_rel_boxes):
            return DragonMatcher.no_match(len(priors), dtype=priors.dtype, device=priors.device)

        overlaps = jaccard(
            gt_rel_boxes,
            xywh_to_xyxy(priors)
        )
        # [1,num_objects] best prior for each ground truth
        best_prior_overlap, best_prior_idx = overlaps.max(1, keepdim=True)
        # [1,num_priors] best ground truth for each prior
        best_truth_overlap, best_truth_idx = overlaps.max(0, keepdim=True)
        best_truth_idx.squeeze_(0)
        best_truth_overlap.squeeze_(0)
        best_prior_idx.squeeze_(1)
        best_prior_overlap.squeeze_(1)
        best_truth_overlap.index_fill_(0, best_prior_idx, 2)  # ensure best prior
        # TODO refactor: index  best_prior_idx with long tensor
        # ensure every gt matches with its prior of max overlap

        for j, idx in enumerate(best_prior_idx):
            best_truth_idx[idx] = j

        # brodcast scatters -
        # Shape: [num_priors,4]
        # NOTE (AJB) - this puts the first ground_truth box as loc box if there was no match
        # if you do not change the implementation this has no side-effect
        matches = gt_rel_boxes[best_truth_idx]
        loc = encode(matches, priors, variances)

        # Shape: [num_priors]
        conf = labels[best_truth_idx]

        # below iou are unmatched
        unmatched = best_truth_overlap < threshold

        # BEGIN ALGO ADDITIONS:

        # store matches until next call (for debug)
        self._match_store['match'] = xywh_to_xyxy(priors[~unmatched, :])

        # special consideration for small boxes too
        gt_rel_boxes_area = ((gt_rel_boxes[:, 2]-gt_rel_boxes[:, 0]) *
                             (gt_rel_boxes[:, 3]-gt_rel_boxes[:, 1]))

        small_gt_idx = torch.nonzero(gt_rel_boxes_area < self._small_box_rel_area)
        if len(small_gt_idx):
            # some hackish masking action - mark unmatched,
            # and those that will not be matched by even the relaxed thresh
            best_truth_idx[best_truth_overlap < self._min_iou_for_small_box_match] = -1
            matched_gt_was_small = torch.zeros_like(unmatched)
            for idx in small_gt_idx:
                matched_gt_was_small |= (best_truth_idx == idx)

            low_thresh = self._small_prior_indexes & matched_gt_was_small

            self._match_store['low_thresh'] = xywh_to_xyxy(priors[low_thresh, :])

            unmatched = unmatched & ~matched_gt_was_small

        # END ALGO ADDITIONS

        # mask out unmatched:
        conf[unmatched] = 0  # label as background

        return loc, conf

    def match_info(self, batch_index=-1):
        data = []
        for label, tensor in self._match_store.items():
            boxes = tensor.cpu().numpy()
            if len(boxes):
                minfo = np.zeros((boxes.shape[0], boxes.shape[1]+2), dtype=boxes.dtype)
                minfo[:, 1:5] = boxes
                minfo[:, 0] = batch_index
                minfo[:, -1] = self._match_label2id.get(label)
                data.append(minfo)
        if len(data):
            return np.vstack(data)
        else:
            return np.empty((0, 6))

    @classmethod
    def estimate_small_box_threshold(cls, priors):
        priors_area = priors[:, 2] * priors[:, 3]
        min_area = torch.min(priors_area).item()
        priors_should_match = priors_area < MAGIC_THRESHOLD * min_area
        return min_area / MAGIC_THRESHOLD, priors_should_match

    @classmethod
    def no_match(cls, num_priors, dtype=torch.float32, device=torch.cuda.current_device()):
        loc = torch.zeros((num_priors, 4), dtype=dtype, device=device)
        conf = torch.zeros((num_priors), dtype=dtype, device=device)
        return loc, conf

