import torch.nn as nn
from torchvision.models.vgg import make_layers, cfgs as vgg_cfgs
from layers.functions.prior_box import NEW_ASPECT_RATIO_KEY, PriorBoxBase

from models.ssd import SKIP, CODE_SIZE, EXTRAS, SSD, calculate_priors_per_scale_for_v1_design, add_extras, \
    add_extras_512


BASE = {
    'vgg16': vgg_cfgs['D'][:-1],
    'vgg19': vgg_cfgs['E'][:-1],
}


# This function is using torchvision VGG make_layers()
# https://github.com/pytorch/vision/blob/master/torchvision/models/vgg.py
def vgg(cfg, batch_norm=False):
    # Creating feature_extraction layers
    feature_extraction = make_layers(cfg=cfg, batch_norm=batch_norm)
    layers = [layer for n, layer in enumerate(feature_extraction.modules()) if type(layer) != nn.Sequential]

    # Creating classification layers
    pool5 = nn.MaxPool2d(kernel_size=3, stride=1, padding=1)
    conv6 = nn.Conv2d(512, 1024, kernel_size=3, padding=6, dilation=6)
    conv7 = nn.Conv2d(1024, 1024, kernel_size=1)
    layers += [pool5, conv6,
               nn.ReLU(inplace=True), conv7, nn.ReLU(inplace=True)]

    return layers


def multibox_vgg(vgg, extra_layers, priors_per_scale, num_classes, vgg_source_index):
    loc_layers = []
    conf_layers = []
    out_channel_list = [vgg[ind].out_channels for ind in vgg_source_index]
    out_channel_list += [l.out_channels for l in extra_layers[1::2]]
    assert len(priors_per_scale) == len(out_channel_list), "priors_per_scale config error"
    for n_priors, FE_out_chan in zip(priors_per_scale, out_channel_list):
        if not n_priors:
            loc_layers += [SKIP]
            conf_layers += [SKIP]
            continue
        loc_layers += [nn.Conv2d(FE_out_chan, n_priors * CODE_SIZE, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(FE_out_chan, n_priors * num_classes, kernel_size=3, padding=1)]
    return vgg, extra_layers, (loc_layers, conf_layers)


def build_ssd_with_vgg(cfg, phase, variant=300, size=(300, 300),
                       num_classes=21, vgg_type='vgg16', dropout=0.1, logger=None):
    if phase != "test" and phase != "train":
        print("ERROR: Phase: " + phase + " not recognized")
        return
    if vgg_type == "vgg16":
        vgg_source = [21, -2]
        conv4_3 = 23
    elif vgg_type == "vgg19":
        vgg_source = [25, -2]
        conv4_3 = 27
    else:
        raise ValueError("vgg_type: " + vgg_type + " not recognized. We support only 'vgg16' or 'vgg19'")

    is_v1 = PriorBoxBase.detect_prior_v1(cfg=cfg)
    if is_v1:
        adaptive = NEW_ASPECT_RATIO_KEY in cfg.keys()
        cfg, priors_per_scale = calculate_priors_per_scale_for_v1_design(cfg, backwards_compatible=(not adaptive))
    else:
        prior_design = cfg['prior_design']
        priors_per_scale = PriorBoxBase.calculate_priors_per_scale(prior_design=prior_design)

    add_extras_func = {300: add_extras, 512: add_extras_512}[variant]

    base_, extras_, head_ = multibox_vgg(vgg=vgg(BASE[str(vgg_type)]),
                                         extra_layers=add_extras_func(EXTRAS[str(variant)], i=1024),
                                         priors_per_scale=priors_per_scale,
                                         num_classes=num_classes,
                                         vgg_source_index=vgg_source)
    return SSD(phase, size, base_, extras_, head_,
               num_classes=num_classes, cfg=cfg, conv4_3=conv4_3, dropout=dropout, logger=logger)
