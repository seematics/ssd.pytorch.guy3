from collections import namedtuple
from functools import partial
from pathlib import Path
from typing import Dict, Tuple

import numpy as np
import pandas
import yaml
from allegroai import Task, DatasetVersion, DataView, IterationOrder, InputModel
from allegroai.debugging import get_logger
from allegroai.utilities.common_dataset_labels import VOC_LABELS
from allegroai.utilities.images import ResizeStrategy
from sklearn.cluster import KMeans
from tqdm.autonotebook import tqdm

from common.prior_optimization.bbox_clustering import ClusteringTaskParameters, get_all_bboxes_in_n_frames, \
    area_from_df, aspect_ratio_from_df, humanized_aspect_ratio_from_df, create_match_scales_logic, create_prior_df, \
    index_to_prior_name_map, emit_prior_wh_basis_from_net, one_minus_one_to_many_iou
from common.prior_optimization import AlignTypeEnum, BoxAlignCalculator, KEEP_DATA
from common.prior_optimization.optimize_priors import intersect_prior_with_gt_bbox_df
from common.utils import ImageSizeTuple, get_model_url


def main():
    task = Task.current_task(default_project_name='Prior Box Clustering 101', default_task_name='Beta attempts')
    logger = task.get_logger()
    # noinspection PyArgumentList
    task_options = ClusteringTaskParameters(
        n_samples=2000,
        max_n_clusters=8,
        min_n_clusters=3,
        resize_w=512,
        resize_h=512,
        upload_destination='s3://allegro-examples',
        feature_extraction_type='vgg16',
    )
    task.connect(task_options)
    target_image_size = ImageSizeTuple(w=task_options.resize_w, h=task_options.resize_h)

    pre_collected_in_data = Path('debug_data_query.pkl')
    if not pre_collected_in_data.exists():
        logger.console("Collecting samples from {} frames".format(task_options.n_samples))
        # todo - dataset specific queries
        # label_queries = default_query() if 'PASCAL' in DATASETVERSIONS[0].dataset_name else {'*'}
        label_queries = {'*': (-1, 0)}
        query_dataview = default_dataview(label_queries=label_queries)
        query_dataview.connect(task)

        # TODO - argument selector between these
        if SCANNING_MODE:
            per_frame_options = dict(
                crop_width=target_image_size.w,
                crop_height=target_image_size.h,
            )
        elif all([size > 0 for size in target_image_size]):
            per_frame_options = dict(
                target_width_height=target_image_size,
                resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO,
            )
        else:
            per_frame_options = None

        if not query_dataview.get_labels():
            logger.warn('No labels in dataview, so label data will not be collected')
        gt_bbox_df = get_all_bboxes_in_n_frames(dataview=query_dataview,
                                                frame_kwargs=per_frame_options,
                                                n_samples=task_options.n_samples)
        logger.console("Filtering duplicates...")
        orig_len = len(gt_bbox_df)
        gt_bbox_df.drop_duplicates(inplace=True)
        logger.console("Dropped {} duplicates. Currently {} bboxes from {} requested frame samples".format(
            orig_len - len(gt_bbox_df), len(gt_bbox_df), task_options.n_samples
        ))

        gt_bbox_df['area'] = area_from_df(gt_bbox_df)
        gt_bbox_df['aspect_ratio'] = aspect_ratio_from_df(gt_bbox_df)
        gt_bbox_df['humanized_aspect_ratio'] = humanized_aspect_ratio_from_df(gt_bbox_df)
        if KEEP_DATA:
            gt_bbox_df.to_pickle(path=pre_collected_in_data)

        if (gt_bbox_df['label'] > 0).any():
            gt_bbox_df['label'].plot(kind='hist', bins=range(len(label_queries)))
    else:
        logger.console('using pre-collected query data in {}'.format(pre_collected_in_data))
        gt_bbox_df = pandas.read_pickle(pre_collected_in_data)

    pre_collected_prior_data = (Path('debug_prior_data_ssd.pkl'), Path('debug_feature_data_ssd.pkl'))
    orig_priors_guess_df, feature_sizes = generate_prior_df_from_ssd_net(
        task=task,
        task_options=task_options,
        target_image_size=target_image_size,
        logger=logger,
        pre_collected_data=pre_collected_prior_data,
    )

    low_bound = create_match_scales_logic(feature_sizes, gt_bbox_df, logger)

    priors_guesses = dict()

    for n_clusters in range(task_options.min_n_clusters, task_options.max_n_clusters+1):
        clusters = []
        for mg, match_group in enumerate(tqdm(low_bound, desc='calculating bbox hierarchy for each match group')):
            # var1 = 'area'
            # var2 = 'aspect_ratio'
            var1 = 'width'
            var2 = 'height'
            var1_view = 'width'
            var2_view = 'height'
            pvars = [var1, var2]
            this_group = gt_bbox_df[match_group].drop_duplicates()
            points = this_group[pvars].values
            effective_n_clusters = min(len(this_group), n_clusters)
            # # create kmeans object
            if effective_n_clusters < n_clusters:
                logger.warn('Not enough samples in match group for n_clusters = {task}, reducing to {this}'.format(
                    task=n_clusters, this=effective_n_clusters))
            # find clusters
            if effective_n_clusters > 0:
                kmeans = KMeans(n_clusters=effective_n_clusters, init='random', verbose=False)
                kmeans.fit(points)
                k_clusters = np.around(kmeans.cluster_centers_).astype(np.float)
                clusters.append((mg, k_clusters))
                if VISUALIZE_CLUSTERING:

                    # # fitkmeans object to data
                    plt.figure()
                    # # print location of clusters learned by kmeans object
                    for i, cluster in enumerate(kmeans.cluster_centers_):
                        logger.console('cluster center {} : {} , {}'.format(i, *cluster.astype(np.int)))
                    # # save new clusters for chart
                    y_km = kmeans.predict(points)
                    for i in range(effective_n_clusters):
                        color = 'C' + str(i)
                        plt.scatter(this_group[var1_view][y_km == i],
                                    this_group[var2_view][y_km == i], s=25, c=color)
                    plt.xlabel(var1_view)
                    plt.ylabel(var2_view)
                    plt.title('K-means Clustering {} vs {} with {} clusters'.format(var1_view, var2_view, n_clusters))
                    ptr = plt.scatter(*k_clusters.T, s=150, marker='X', edgecolors='black')
            else:
                logger.warn("no cluster for match group!")
                clusters.append((mg, []))

        if VISUALIZE_CLUSTERING:
            plt.show()

        all_clusters = np.vstack([cluster_group for _, cluster_group in clusters if len(cluster_group)])
        match_groups = np.hstack([[mg] * len(cluster_group) for mg, cluster_group in clusters if len(cluster_group)])
        current_priors_guess_df = create_prior_df(all_clusters, match_groups)
        priors_guesses[n_clusters] = current_priors_guess_df

    # WIP - each partly done:
    # prior on the priors (suggest priors)
    # warn (and prune) too small bboxes w.r.t smallest prior (maybe not?)
    # filter using iou with random shifts to simulate real-world occurences

    # ## Zero PASS - Baseline == Max possible iou between old prior and gt bbox ###
    orig_name_mapper = index_to_prior_name_map('{}_iou', orig_priors_guess_df)

    orig_max_iou_with_priors, orig_percent_matched = intersect_prior_with_gt_bbox_df(
        box_aligner=BoxAlignCalculator(align_strategy=AlignTypeEnum.BottomLeft),
        iou_min_threshold=IOU_THRESHOLD_FOR_PRIOR_MATCH,
        priors_df=orig_priors_guess_df,
        gt_bbox_df=gt_bbox_df,
        column_name_map=orig_name_mapper,
        precalc_score_df=None,
        precalc_score_name_map=None,
        vis_options=None,
        logger=logger,
        debug_result_data=Path('iou_prev_pass_result.pkl')
    )

    priors_to_index = {'prior_{}'.format(n): n for n in range(len(orig_priors_guess_df))}
    # add the argmax:
    name_to_index = {column_name: priors_to_index[prior_name]
                     for prior_name, column_name in orig_name_mapper.items()}
    orig_best_match_1st = orig_max_iou_with_priors.idxmax(axis=1).map(name_to_index)
    orig_num_matches_per_bbox_1st = orig_max_iou_with_priors.apply(
        lambda t: np.count_nonzero(t > IOU_THRESHOLD_FOR_PRIOR_MATCH), axis=1)

    x = orig_best_match_1st.dropna().astype(np.int)
    orig_priors_use_freq, _ = np.histogram(x, bins=len(orig_priors_guess_df), range=(0, len(orig_priors_guess_df)))

    mean_match_iou_orig = orig_max_iou_with_priors.mean(axis=1)
    num_unmatched_orig = mean_match_iou_orig.isna().sum()
    logger.console(" for original priors with mean match iou {}, "
                   " there are {} unmatched bboxes".format(mean_match_iou_orig.mean(), num_unmatched_orig))

    max_iou_w_prior_n_cluster = dict()
    percent_matched_n_cluster = dict()
    best_match_1st = dict()
    num_matches_per_bbox_1st = dict()
    priors_use_freq = dict()
    priors_per_mg = dict()
    mean_match_iou = dict()
    num_unmatched_n_cluster = dict()

    for key in sorted(priors_guesses.keys()):
        priors_guess_df = priors_guesses.get(key)
        priors_per_mg[key] = [priors_guess_df[priors_guess_df['match_group'] == mg].index.tolist()
                              for mg in set(priors_guess_df['match_group'])]
        name_mapper = index_to_prior_name_map('{}_iou%s'%IOU_THRESHOLD_FOR_PRIOR_MATCH, priors_guess_df)
        max_iou_w_prior_n_cluster[key], percent_matched_n_cluster[key] = intersect_prior_with_gt_bbox_df(
            box_aligner=BoxAlignCalculator(align_strategy=AlignTypeEnum.BottomLeft),
            iou_min_threshold=IOU_THRESHOLD_FOR_PRIOR_MATCH,
            priors_df=priors_guess_df,
            gt_bbox_df=gt_bbox_df,
            column_name_map=name_mapper,
            precalc_score_df=None,
            precalc_score_name_map=None,
            vis_options=None,
            logger=logger,
            debug_result_data=Path('iou_pass_result_n_clusters_{}.pkl'.format(key))
        )
        priors_to_index = {'prior_{}'.format(n): n for n in range(len(priors_guess_df))}
        # add the argmax:
        name_to_index = {column_name: priors_to_index[prior_name]
                         for prior_name, column_name in name_mapper.items()}
        best_match_1st[key] = max_iou_w_prior_n_cluster[key].idxmax(axis=1).map(name_to_index)

        num_matches_per_bbox_1st[key] = max_iou_w_prior_n_cluster[key].apply(
            lambda t: np.count_nonzero(t > IOU_THRESHOLD_FOR_MAX_PRIOR_MATCH), axis=1)

        # num unmatched
        mean_match_iou[key] = max_iou_w_prior_n_cluster[key].mean(axis=1)
        num_unmatched_n_cluster[key] = mean_match_iou[key].isna().sum()
        logger.console(" for {} max clusters per match group with mean match iou {}, "
                       " there are {} unmatched bboxes".format(key,
                                                               mean_match_iou[key].mean(),
                                                               num_unmatched_n_cluster[key]))

        x = best_match_1st[key].dropna().astype(np.int)
        priors_use_freq[key], _ = np.histogram(x, bins=len(priors_guess_df), range=(0, len(priors_guess_df)))

    percent_matched_summary = \
        pandas.DataFrame(percent_matched_n_cluster,
                         index=percent_matched_n_cluster[max(percent_matched_n_cluster.keys())].index)
    num_matches_per_bbox_summary = pandas.DataFrame(num_matches_per_bbox_1st)
    best_matched_summary = pandas.DataFrame(best_match_1st)

    # second_stage_column_name = '{}_avg_iou'
    # second_stage_name_mapper = index_to_prior_name_map(second_stage_column_name, priors_guess_df)
    # vis_options = create_default_vis_options()
    # vis_options['viz_flag'] = False
    # vis_options['min_iou_to_viz'] = IOU_THRESHOLD_FOR_MAX_PRIOR_MATCH
    #
    # some_iou_with_priors, second_stage_percent_matched = intersect_prior_with_gt_bbox_df(
    #     box_aligner=BoxAlignCalculator(align_strategy=AlignTypeEnum.MultiRandom),
    #     iou_min_threshold=IOU_THRESHOLD_FOR_PRIOR_MATCH,
    #     priors_df=priors_guess_df,
    #     gt_bbox_df=gt_bbox_df,
    #     column_name_map=second_stage_name_mapper,
    #     precalc_score_df=max_iou_with_priors,
    #     precalc_score_name_map=first_stage_name_mapper,
    #     vis_options=vis_options,
    #     logger=logger,
    #     debug_result_data=Path('iou_second_pass_result.pkl')
    # )
    #
    # name_to_index = {column_name: priors_to_index[prior_name]
    #                  for prior_name, column_name in second_stage_name_mapper.items()}
    # best_match_2nd = some_iou_with_priors.idxmax(axis=1).map(name_to_index)
    # num_matches_per_bbox_2nd = some_iou_with_priors.apply(
    #     lambda t: np.count_nonzero(t > IOU_THRESHOLD_FOR_PRIOR_MATCH), axis=1)

    logger.console('hmmm')


SCANNING_MODE = True
ZEROTH_THRESHOLD_FOR_PRIOR_MATCH = 0.2
IOU_THRESHOLD_FOR_MAX_PRIOR_MATCH = 0.50
IOU_THRESHOLD_FOR_PRIOR_MATCH = 0.40
VISUALIZE_CLUSTERING = True
if VISUALIZE_CLUSTERING:
    from matplotlib import pyplot as plt
DATASETVERSIONS = [
    # DatasetVersion.get_version(
    #     dataset_name='MAFAT Challenge - objects from aerial imagery',
    #     version_name='Training imagery'),
    # DatasetVersion.get_version(
    #     dataset_name='MAFAT Challenge - objects from aerial imagery',
    #     version_name='Training-imagery-split-training-subset'),
    DatasetVersion.get_version(
        dataset_name='PASCAL Visual Object Classes',
        version_name='Train2012 version'),
    DatasetVersion.get_version(
        dataset_name='COCO - Common Objects in Context',
        version_name='Train2017 version'),
]
dataset_version = namedtuple('DatasetVersionTuple', 'dataset version')


def default_dataview(label_queries: Dict[str, Tuple[int, int]] = None):
    new_dataview = DataView(iteration_infinite=False, iteration_order=IterationOrder.random)
    seed = Task.current_task().get_random_seed()
    Task.current_task().get_logger().console("random seed from task: {}".format(seed))
    new_dataview.set_random_seed(seed)
    if label_queries:
        for version in DATASETVERSIONS:
            for label, data in label_queries.items():
                new_dataview.add_query(
                    dataset_name=version.dataset_name,
                    version_name=version.version_name,
                    weight=data[1] if data[1] > 0 else 1.0,
                    roi_query=label,
                )
        labels = {key: data[0] for key, data in label_queries.items() if key is not '*'}
        if len(labels):
            new_dataview.set_labels(labels)
    else:
        for version in DATASETVERSIONS:
            new_dataview.add_query(
                dataset_name=version.dataset_name,
                version_name=version.version_name,
            )

    return new_dataview


def generate_prior_df_from_ssd_net(
        task: Task,
        task_options: ClusteringTaskParameters,
        target_image_size: ImageSizeTuple,
        logger=None,
        pre_collected_data=None,
):
    logger = logger if logger else get_logger()

    # create priors -> get from ssd
    if all(data.exists() for data in pre_collected_data):
        logger.console('using pre-collected prior data in {}'.format(",".join([str(p) for p in pre_collected_data])))
        return (pandas.read_pickle(data) for data in pre_collected_data)

    logger.console('Need a model+design to construct the SSD network...')
    input_model_url, input_model_name = \
        get_model_url(task_options.feature_extraction_type)
    input_label_enumeration = {k: v[0] for k, v in VOC_LABELS.items()}
    default_design_file = Path(__file__).parents[1] / Path('ssd_design.yaml')
    # default_design_file = Path(__file__).parents[1] / Path('ssd_design_manual.yaml')

    input_model = InputModel.import_model(weights_url=input_model_url, name=input_model_name,
                                          design=default_design_file.read_text(),
                                          label_enumeration=input_label_enumeration)
    task.connect(input_model)
    design = input_model.design
    if not design:
        logger.console('Design not found in input model. Trying default design...')
        if not default_design_file.exists():
            raise IOError('Missing default model design file in repo: {}'.format(default_design_file))
        else:
            design = default_design_file.read_text()
    ssd_config_params = yaml.load(design)

    ssd_priors, match_group_info, fmaps = \
        emit_prior_wh_basis_from_net(task_options, ssd_config_params, target_image_size, logger=logger)
    priors_guess = create_prior_df(ssd_priors, match_group_info)

    fmap_sizes = pandas.DataFrame(fmaps, columns=['width', 'height'])
    if KEEP_DATA and pre_collected_data:
        priors_guess.to_pickle(path=pre_collected_data[0])
        fmap_sizes.to_pickle(path=pre_collected_data[1])
    return priors_guess, fmap_sizes


def map_voc_to_coco():
    return NotImplementedError


def default_query():
    return {key: (val[0], 0) for key, val in VOC_LABELS.items() if val[0] > 0}
    # TODO - switch case
    # return {key: (val[0], 0) for key, val in COCO_LABELS.items() if val[0] > 0}


def k_means_custom_metric(boxes, k, starting_guess=None, dist_func=np.median, custom_metric=None):
    # based on https://github.com/lars76/kmeans-anchor-boxes
    """
    Calculates k-means clustering with the a custom metric.
    :param starting_guess:
    :param custom_metric: Calculate distances matrix row by row, argument is a tuple (box, other boxes)
    :param boxes: numpy array of shape (r, 2), where r is the number of rows
    :param k: number of clusters
    :param dist_func: distance function
    :return: numpy array of shape (k, 2)
    """

    custom_metric = custom_metric if custom_metric else \
        partial(one_minus_one_to_many_iou, box_aligner=BoxAlignCalculator(align_strategy=AlignTypeEnum.BottomLeft))

    max_iter = 1000
    rows = boxes.shape[0]
    if starting_guess:
        assert len(starting_guess) == k, "duhhh"

    distances = np.empty((rows, k))
    last_clusters = np.zeros((rows,))

    np.random.seed(seed=Task.current_task().get_random_seed())

    # the Forgy method will fail if the whole array contains the same rows
    clusters = starting_guess or boxes[np.random.choice(rows, k, replace=False)]

    iter = 0
    while iter < max_iter:
        for n, box in enumerate(boxes):
            distances[n] = custom_metric((box, clusters))
        nearest_clusters = np.argmin(distances, axis=1)

        if (last_clusters == nearest_clusters).all():
            break

        for cluster in range(k):
            clusters[cluster] = dist_func(boxes[nearest_clusters == cluster], axis=0)

        last_clusters = nearest_clusters
        iter += 1

    return clusters