import torch
import numpy as np
import cv2
import time

from functools import partial
from logging import ERROR
from torch.autograd import Variable

from layers import Detect

from common.ssd_pipeline import make_ssd_batch
from common.metrics import BoxesHistory, average_precision_from_curve
from common.utils import ssd_output_to_allegro_format, ImageSizeTuple

from allegroai.imageframe import ResizeStrategy
from allegroai import DataPipe

# epsilon for numerical methods:
EPS = np.finfo(float).eps

# softmax module for the ssd train and test
softmax = torch.nn.Softmax(dim=-1)


def test_model(net, config_params, args, test_dataview, test_mapping, test_draw_func, logger, at_iteration=0,
               cuda_on=True):
    # hardcoded parameters:
    precision_recall_conf_list = [0.001, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 0.999]
    min_precision_for_integrated_metric = 0.7

    target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])
    decode_boxes = Detect(num_classes=config_params['num_classes'], bkg_label=0,
                          top_k=config_params.get('top_k', 200),
                          conf_thresh=config_params.get('conf_thresh', 0.01),
                          nms_thresh=config_params['nms_thresh'], cfg=config_params)

    if args.test_size <= 0 or not test_dataview:
        logger.console('Skipping test, test_size=0')
        return

    # Set net to eval mode for testing
    net.eval()

    logger.console('%s' % str(80 * '-'))
    t_test = time.time()
    iou = [i for i in config_params['test_detection_iou_thresh']]

    batcher = partial(make_ssd_batch, target_image_size_wh=target_image_size)

    iterator = test_dataview.get_iterator()

    test_pipe = DataPipe(iterator, num_workers=args.num_workers,
                         frame_cls_kwargs=dict(target_width_height=target_image_size,
                                               resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO))
    test_pipe_iterator = test_pipe.get_iterator()
    logger.console('Testing currently loaded weights...')

    box_history = BoxesHistory()

    pre_batch_collect = []
    last_key_frame = False
    all_batch_boxes = None
    all_batch_scores = None
    flt_mean = 0
    report_iter = 0
    for test_iter, image_frame in enumerate(test_pipe_iterator):
        if test_iter > args.test_size:
            break

        pre_batch_collect.append(image_frame)
        if len(pre_batch_collect) != args.batch_size:
            continue

        # collect the frames into a batch (i.e. a list of tuples)
        image_batch = pre_batch_collect
        pre_batch_collect = []
        # use the batch and zero it at the end

        # test batch
        batch = batcher(image_batch)
        batch_input = [torch.from_numpy(inp) for inp in batch.input]
        batch_input = [Variable(inp.cuda()) if cuda_on else Variable(inp) for inp in batch_input]

        # forward
        out = net(*batch_input)
        pred_probs = softmax(out[1])
        detections = decode_boxes(out[0], pred_probs, out[2])
        detections = detections.clamp(0., 1.)
        detections = detections.data.cpu().numpy()
        all_batch_boxes, all_batch_scores = \
            ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                         conf_thresh=min(precision_recall_conf_list),
                                         img_width=target_image_size.w,
                                         img_height=target_image_size.h)

        # Make sure we have hard rois
        all_ground_truth = tuple(bb for bb in (batch.ground_truth, batch.hard_ground_truth) if bb.size)
        if all_ground_truth:
            all_ground_truth = np.vstack(all_ground_truth)
            box_history.save_frame_data(pred_boxes=all_batch_boxes, pred_scores=all_batch_scores,
                                        gt_boxes=all_ground_truth)
        else:
            all_ground_truth = None

        report_iter += 1
        if report_iter and report_iter % args.report_iterations == 0:
            msg = 'Test iteration %-6d' % report_iter
            logger.console(msg)
        if report_iter and report_iter % (args.report_iterations * args.report_images_every_n_reports) == 0:
            t_images = time.time()
            data_name = ['Images']
            img_to_draw = []
            for input_type in batch.input:
                if input_type.shape[1] == 4:
                    first_three = input_type[:, :3, :, :]
                    first_three = first_three.transpose(0, 2, 3, 1)
                    img_to_draw.append(first_three)

                    last = input_type[:, 3:, :, :]
                    last = np.repeat(last, 3, axis=1)
                    last = last.transpose(0, 2, 3, 1)
                    img_to_draw.append(last)
                elif input_type.shape[1] == 3:
                    np_images = input_type.transpose(0, 2, 3, 1)
                    img_to_draw.append(np_images)
                else:
                    raise ValueError('Only 3,4 channels are supported')

            if len(data_name) < len(batch.input):
                logger.console('Too many inputs, not designed for such amount, using default names', level=ERROR)
                data_name.extend(['']*len(batch.input))
            for stage_id, np_images in enumerate(img_to_draw):
                draws = test_draw_func(images=np_images,
                                       pred_scores=all_batch_scores,
                                       pred_boxes=all_batch_boxes,
                                       gt_boxes=all_ground_truth,
                                       gt_boxes_labels=batch.ground_truth_labels,
                                       gt_boxes_hard=batch.hard_ground_truth_boxes,
                                       gt_boxes_hard_labels=batch.hard_ground_truth_labels)

                draws = [cv2.putText(im, 'TEST', (10, 30), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
                         for im in draws]

                for idx, img in enumerate(draws):
                    logger.report_image_and_upload(title='Test - %s' % data_name[stage_id], series='test_img_%d' % idx,
                                                   iteration=at_iteration, matrix=img.astype(np.uint8))

            t_images = time.time() - t_images
            logger.console('Iter %d Time to create test images: %.4f (sec)' % (report_iter, t_images))
            logger.flush()

    # Report results
    id_to_label = {v: k for k, v in test_mapping.items() if v > 0}
    logger.console('Calculating precision and creating report...')

    for iou_thresh in iou:
        pr_curves_per_label = box_history.get_precision_recall_curve(
            iou_threshold=iou_thresh,
            conf_thresholds=precision_recall_conf_list,
            nms_threshold=config_params.get('nms_thres_for_metrics', 0.65))

        all_ap = []
        all_neap = []
        simple_title = '{} | IOU=%3.2f' % iou_thresh
        for id in pr_curves_per_label:
            pr_curve = pr_curves_per_label[id]
            label_str = id_to_label.get(id, 'Label %d' % id)
            ap = average_precision_from_curve(pr_curve)
            ap_with_min_prec = average_precision_from_curve(pr_curve, min_precision=min_precision_for_integrated_metric)
            prec_rec_title = '[{label_str}] Precision-Recall @IOU:{iou_thresh}'
            conf_f1_title = '[{label_str}] Conf-F1-score @IOU:{iou_thresh}'
            min_prec = min_precision_for_integrated_metric
            series_title = 'it: {at_iteration} ap: {ap:3.2f} nEmAP_{min_prec:3.2f}: {ap_with_min_prec:3.2f}'

            prec_recall_graph = [(recall, precision) for _, precision, recall in pr_curve.raw_curve]
            conf_fscore_graph = [(conf, ((2 * recall * precision) / (EPS + recall + precision)))
                                 for conf, precision, recall in pr_curve.raw_curve]
            top_fscore = max([x[1] for x in conf_fscore_graph])
            logger.console('AP=%.2f AP_with_min_prec=%.2f' % (ap, ap_with_min_prec))
            logger.report_scatter2d(
                labels=['conf_thr=%3.2f' % x[0] for x in conf_fscore_graph],
                title=prec_rec_title.format(**locals()),
                series=series_title.format(**locals()),
                iteration=-1,
                xaxis='Recall',
                yaxis='Precision',
                scatter=prec_recall_graph)

            logger.report_scatter2d(
                labels=['Recall=%3.2f, Precision=%3.2f' % (x[0], x[1]) for x in prec_recall_graph],
                title=conf_f1_title.format(**locals()),
                series=series_title.format(**locals()),
                iteration=-1,
                xaxis='Conf. Threshold',
                yaxis='F1-Score',
                scatter=conf_fscore_graph)

            logger.report_scalar(
                title=simple_title.format('Top F1-score'),
                series='%s' % label_str,
                iteration=at_iteration,
                value=top_fscore)
            logger.report_scalar(
                title=simple_title.format('normalized Effective AP_%3.2f' % min_prec),
                series='%s' % label_str,
                iteration=at_iteration,
                value=ap_with_min_prec)
            all_neap.append(ap_with_min_prec)
            logger.report_scalar(
                title=simple_title.format('Average Precision'),
                series='%s' % label_str,
                iteration=at_iteration,
                value=ap)
            all_ap.append(ap)

            logger.console(simple_title.format('Precision-Recall'))
            logger.console(prec_recall_graph)
            logger.console('-------------------------------------------------')
            logger.console(simple_title.format('Conf-F1-score'))
            logger.console(conf_fscore_graph)
            logger.console('-------------------------------------------------')

        logger.report_scalar(
            title=simple_title.format('normalized Effective AP_%3.2f' % min_precision_for_integrated_metric),
            series='mnEAP_%3.2f' % min_precision_for_integrated_metric,
            iteration=at_iteration,
            value=np.mean(all_neap))
        logger.report_scalar(
            title=simple_title.format('Average Precision'),
            series='mAP',
            iteration=at_iteration,
            value=np.mean(all_ap))

        logger.flush()

    logger.console('Finished testing snapshot!')
    logger.console('%s' % str(80 * '-'))
    msg = 'Iteration %-6d : test time %.3f (sec)' % (at_iteration, time.time() - t_test)
    logger.console(msg)
    logger.flush()
