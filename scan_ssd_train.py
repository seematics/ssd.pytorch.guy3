import time
from argparse import ArgumentParser
from functools import partial
from logging import ERROR, WARNING

import numpy as np
import torch
import yaml
from allegroai import DataView, Augmentation, DataPipe, Task, InputModel, OutputModel, ImageFrame, IterationOrder
from allegroai.debugging import Timer
from allegroai.utilities.augmentations import CustomAugmentationZoom
from allegroai.utilities.images import ResizeStrategy
from pathlib2 import Path

from common.augmentations import CustomAugmentationFixedCropSizeWithProb
from common import TIMERS_BENCHMARK_WINDOW, LOSS_AVERAGING_WINDOW
from common.loading import default_weight_init_for_module
from common.prior_optimization import PriorOptimizationOptions
from common.prior_optimization.optimize_priors import optimize_priors
from common.ssd_pipeline import make_ssd_batcher, make_ssd_get_item
from common.utils import ssd_output_to_allegro_format, setup_pytorch, ImageSizeTuple, upload_model_snapshot, \
    resolve_resize_strategy, prior_output_to_allegro_format, CyclicLRScheduler, freeze_first_n_layers, \
    make_deterministic, get_model_url, check_labels, bound_number_type, build_ssd, setup_keepalive_logger, nan_safe_item
from common.visualization import DrawBoxesAndLabels
from layers import DragonLoss
from layers.functions.prior_box import NEW_ASPECT_RATIO_KEY
from scan_ssd_test import test_model

# =================   GLOBAL PARAMETERS  ==============================
# Help reuse task id when in development mode
TASK_NAME = 'Train SSD [scan mode] example'
PROJECT_NAME = 'pytorch ssd'

# Debug timers
step_timer = Timer()
# convert_to_torch = Timer()
total_timer = Timer()


def get_parser(input_parser=None):
    parser = input_parser or ArgumentParser(
        description='Single Shot Dragon Detector Training With Pytorch')

    # (0) Basic config
    parser.add_argument('--batch-size', default=6, type=int, help='Number of images per batch')
    parser.add_argument('--max-train-iterations', '--max-iterations', default=150000, type=int,
                        help='Max number of iterations for train')
    parser.add_argument('--save-iterations', default=15000, type=int, help='Save model every K iterations')
    parser.add_argument('--report-iterations', default=100, type=int, help='Report every N Iterations')
    parser.add_argument('--report-images-every-n-reports', default=3, type=int, help='Images report frequency')
    parser.add_argument('--conf-thresh-debug-images', default=0.35, help='Confidence threshold for debug images',
                        type=bound_number_type(minimum=0, maximum=1, dtype=float))
    parser.add_argument('--num-workers', default=32, type=int, help='Number of workers used for parallel data loading')
    parser.add_argument('--upload-destination', default='s3://allegro-tutorials', type=str,
                        help='Destination to upload debug images and models')
    parser.add_argument('--report-debug-info', default=0, type=int, help='Add debug information')

    # (1) Model control
    parser.add_argument('--feature-extraction-type', default='vgg16', type=str,
                        help='Feature extraction net. We support: vgg16, vgg19, squeezenet1_0, squeezenet1_1, '
                             'resnet50, resnet101, mobilenetV2')
    parser.add_argument('--dropout', default=0.1, type=bound_number_type(minimum=0, maximum=1, dtype=float),
                        help='dropout value between 0 and 1')

    # (2) Data control
    parser.add_argument('--source-id', type=str, default=None,
                        help='In case of several sources per frame, only choose this source id to train on')

    # (3) Testing control
    parser.add_argument('--max-test-iterations', '--test-size', default=2000, type=int,
                        help='Max number of iterations for test')
    parser.add_argument('--test-while-train', default=1, type=int, help='Run a test every time a snapshot is saved')
    parser.add_argument('--test-before-train', default=0, type=int, help='Run a test before training')

    # (4) solver control (This task uses SGD)
    parser.add_argument('--end2end-train', default=0, type=int, help='Train full net architecture')
    parser.add_argument('--momentum', default=0.9, type=float, help='SGD momentum')
    parser.add_argument('--weight-decay', default=0.0005, type=float, help='Weight decay coefficient')

    # (5) Pytorch specific
    parser.add_argument('--cuda', default=1, type=int, help='Use CUDA to train model')

    # experimental:
    parser.add_argument('--optimize-priors-sample-size', default=0, type=int,
                        help='[warning: experimental] sample size to based on for generating priors'
                             ' optimized for the dataview')
    parser.add_argument('--equalize_dataview_for_optimize_priors', default=0, type=int,
                        help='Try to add weights to all labels in dataview queries to account for all')

    parser.add_argument('--loss_alpha', default=1, help='Expert: Loss = (alpha+1)/2*cls_loss+loc_loss',
                        type=bound_number_type(minimum=0, maximum=1.01, dtype=float))
    return parser


def train(start_iter, stop_iter):
    logger.console('... Train commence - getting iterator.')

    if args.source_id:
        source_id = [args.source_id]
        if len(source_id) > 1:
            raise ValueError(
                'Currently we do not support training with mutliple sources,'
                ' sorry. (source were: {})'.format(source_id))
    else:
        source_id = None
    resize_strategy = resolve_resize_strategy(args, logger)
    train_iterator = train_dataview.get_iterator()

    pipe = DataPipe(train_iterator,
                    min_queue_depth=max(4, args.num_workers // args.batch_size),
                    batch_size=args.batch_size,
                    num_workers=args.num_workers,
                    get_item_fn=make_ssd_get_item(target_width_height=target_image_size, source_id=source_id),
                    collate_fn=make_ssd_batcher(cuda_on=cuda_on),
                    frame_cls_kwargs=dict(target_width_height=target_image_size,
                                          resize_strategy=resize_strategy,
                                          default_source_ids_to_load=source_id))
    # temporary fix - do not drop entire batch if one image fails to load:
    pipe._batcher_kwargs.update(dict(drop_batch_on_error=False))

    data_loader = pipe.get_iterator()

    net.train()

    loss_equalizer = 0.5 * (float(args.loss_alpha) + 1)
    # loss counters
    cumulative_loc_loss_reported = []
    cumulative_conf_loss_reported = []

    iteration = start_iter
    match_info = None

    logger.console('Got it. Training Start!')
    timing_histogram = None

    # device = torch.cuda.current_device() if cuda_on else torch.device('cpu')

    # print out log messages every interval seconds
    still_alive_msg = "Still training ... [iter: {iter}, avg. step {time} sec]"
    keepalive = setup_keepalive_logger(still_alive_msg.format(iter=iteration, time=0), logger=logger, interval=60.0)

    total_timer.tic()
    for batch in data_loader:
        if iteration > stop_iter:
            break

        # Copy to GPU has moved into the collate function ^

        # convert_to_torch.tic()
        # input_images = batch.torch_input.images.to(device)
        # with torch.no_grad():
        #     targets = [ann.to(device) for ann in batch.torch_input.targets]
        #
        # if args.report_debug_info and cuda_on:
        #     torch.cuda.synchronize()
        #
        # convert_to_torch.toc()

        input_images = batch.torch_input.images
        targets = batch.torch_input.targets

        step_timer.tic()
        # forward
        out = net(input_images)

        optimizer.zero_grad()
        loss_l, loss_c = criterion(out, targets)
        loss = loss_l + loss_equalizer * loss_c

        # back-propagation
        loss.backward()

        # optimization
        lr_scheduler.step()
        optimizer.step()

        if args.report_debug_info and cuda_on:
            torch.cuda.synchronize()

        step_timer.toc()

        # we adopt a convention where the iteration count is "how many steps taken"
        iteration += 1
        keepalive.message = still_alive_msg.format(iter=iteration,
                                                   time=np.around(step_timer.average_time, decimals=2))

        # send statistics
        if iteration and iteration % args.report_iterations == 0:
            this_iter_loss = nan_safe_item(loss)
            this_iter_loss_l = nan_safe_item(loss_l)
            this_iter_loss_c = nan_safe_item(loss_c)

            if args.report_debug_info:
                match_info = criterion.match_info(iteration=iteration, logger=logger)

            # grab loss for this iteration
            if np.isfinite(this_iter_loss_l):
                cumulative_loc_loss_reported.append(this_iter_loss_l)
                cumulative_loc_loss_reported = cumulative_loc_loss_reported[-LOSS_AVERAGING_WINDOW:]
            else:
                logger.console('NOTE: Localization loss was %f in iteration %d' % (this_iter_loss_l, iteration))
            if np.isfinite(this_iter_loss_c):
                cumulative_conf_loss_reported.append(this_iter_loss_c)
                cumulative_conf_loss_reported = cumulative_conf_loss_reported[-LOSS_AVERAGING_WINDOW:]
            else:
                logger.console('NOTE: Classification loss was %f in iteration %d' % (this_iter_loss_c, iteration))

            # Scalar Reporting
            if len(cumulative_loc_loss_reported):
                logger.report_scalar(title='Localization Loss', series='iter. mean', iteration=iteration,
                                     value=sum(cumulative_loc_loss_reported) / len(cumulative_loc_loss_reported))
            logger.report_scalar(title='Localization Loss', series='current', iteration=iteration,
                                 value=this_iter_loss_l)
            if len(cumulative_conf_loss_reported):
                logger.report_scalar(title='Classification Loss', series='iter. mean', iteration=iteration,
                                     value=sum(cumulative_conf_loss_reported) / len(cumulative_conf_loss_reported))
            logger.report_scalar(title='Classification Loss', series='current', iteration=iteration,
                                 value=this_iter_loss_c)
            for idx, param_group in enumerate(optimizer.param_groups):
                logger.report_scalar(title='Learning-Rate', series='Param Group %d' % idx, iteration=iteration,
                                     value=param_group['lr'])

            # Note: iteration time reported as rates
            if total_timer.average_time:
                logger.report_scalar(title='Rates[1/sec]', series='images', iteration=iteration,
                                     value=(len(batch.torch_input.images) / total_timer.average_time))
                logger.report_scalar(title='Rates[1/sec]', series='iterations', iteration=iteration,
                                     value=(1 / total_timer.average_time))
            if TIMERS_BENCHMARK_WINDOW > 0:
                batchstr = ' batch = {}'.format(len(batch.torch_input.images)) if args.report_debug_info else ''
                title = 'Window Average Time (size %d)' % TIMERS_BENCHMARK_WINDOW
                logger.report_scalar(title=title, series='Iter time'+batchstr,
                                     iteration=iteration, value=total_timer.average_time)
                if args.report_debug_info:
                    ssd_times = net.module.get_average_times() if cuda_on else net.get_average_times()
                    if 'fw_time' in ssd_times:
                        logger.report_scalar(title=title, series='fw only time'+batchstr,
                                             iteration=iteration, value=ssd_times['fw_time'])
                        if ssd_times['detect_time'] > 0:
                            logger.report_scalar(title=title, series='detect only time'+batchstr,
                                                 iteration=iteration, value=ssd_times['detect_time'])

            n_report = 7
            # draw histogram of timing last n*2 iterations
            # overhead time:
            overhead = total_timer.average_time - (
                    step_timer.average_time) # + convert_to_torch.average_time)
            this_histogram = np.atleast_2d(np.around(np.array(
                # [convert_to_torch.average_time,
                 [step_timer.average_time,
                  overhead]) / len(batch.torch_input.images), decimals=3)).T
            timing_histogram = np.hstack(
                (timing_histogram[:, -(n_report - 1):],
                 this_histogram)) if timing_histogram is not None else this_histogram
            # histogram_labels = ['convert', 'step', 'overhead']
            histogram_labels = ['step', 'overhead']
            if timing_histogram.shape[-1] >= n_report:
                logger.report_vector(title='Average Timing - last %d reports [sec/image]' % n_report, series='average',
                                     iteration=0, values=timing_histogram, labels=histogram_labels)
                timing_histogram = None

            logger.flush()

            # Image Reporting
            if (iteration // args.report_iterations) % args.report_images_every_n_reports == 0:
                t_images = time.time()
                net.eval()
                conf_thresh = net.module.default_conf_thresh if cuda_on else net.default_conf_thresh
                detections = net(input_images, conf_thresh=conf_thresh)
                detections = detections.data.cpu().numpy()
                all_batch_boxes, all_batch_scores = \
                    ssd_output_to_allegro_format(numpy_ssd_output=detections,
                                                 conf_thresh=args.conf_thresh_debug_images,
                                                 img_width=target_image_size.w,
                                                 img_height=target_image_size.h)

                # PyTorch images are (3, h, w), for cv2 we need: (h, w, 3) transpose
                np_images = batch.np_images.transpose(0, 2, 3, 1)

                if args.report_debug_info and match_info is not None:
                    prior_debug_images = [im.copy() for im in np_images]
                    all_batch_prior_match, fake_match_scores =\
                        prior_output_to_allegro_format(match_info,
                                                       img_width=target_image_size.w,
                                                       img_height=target_image_size.h)
                    debug_images = draw_boxes_with_mapping(images=prior_debug_images,
                                                           pred_boxes=all_batch_prior_match,
                                                           pred_scores=fake_match_scores,
                                                           gt_boxes=batch.ground_truth)
                    for idx, img in enumerate(debug_images):
                        logger.report_image_and_upload(title='prior matching', series='img_%d' % idx,
                                                       iteration=iteration, matrix=img.astype(np.uint8))

                draws = draw_boxes_with_mapping(images=np_images, pred_scores=all_batch_scores,
                                                pred_boxes=all_batch_boxes,
                                                gt_boxes=batch.ground_truth)
                for idx, img in enumerate(draws):
                    logger.report_image_and_upload(title='Images', series='img_%d' % idx,
                                                   iteration=iteration, matrix=img.astype(np.uint8))

                t_images = time.time() - t_images
                logger.console('Time to create debug images: %.4f (sec)' % t_images)
                logger.flush()
                net.train()

            # General log printouts
            msg = 'Iteration %-6d : t_iter %.3f (sec) | Loss %.4f' % \
                  (iteration, total_timer.average_time, this_iter_loss)
            logger.console(msg)
            if TIMERS_BENCHMARK_WINDOW > 0 and (
                    iteration // args.report_iterations) % TIMERS_BENCHMARK_WINDOW == 0:
                    logger.console('Resetting timer averages at iteration %d' % iteration)
                    step_timer.reset_average()
                    # convert_to_torch.reset_average()
                    total_timer.reset_average()
                    net.module.reset_timers() if cuda_on else net.reset_timers()
        total_timer.toc()

        # store snapshot
        if iteration != 0 and iteration % args.save_iterations == 0:
            logger.console('Saving state, iter: %d' % iteration)
            upload_model_snapshot(net=ssd_net, model=output_model, iteration=iteration)
            if test_while_train:
                keepalive.should_stop = True
                test_model(net, config_params, args, test_dataview,
                           test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
                           logger=logger, at_iteration=iteration, cuda_on=cuda_on)
                keepalive = setup_keepalive_logger(still_alive_msg.format(iter=iteration, time=0),
                                                   logger=logger, interval=60.0)
                net.train()

        # Start timimg now so we do not add the test time to the average total time per step
        total_timer.tic()

    # we are done, upload the last snapshot
    upload_model_snapshot(net=ssd_net, model=output_model, iteration=iteration)
    if test_while_train:
        keepalive.should_stop = True
        test_model(net, config_params, args, test_dataview,
                   test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
                   logger=logger, at_iteration=iteration, cuda_on=cuda_on)
    logger.console('All done! Have a nice day!')
    task.mark_stopped()


if __name__ == '__main__':
    ###################################
    # Create task in allegro's system #
    ###################################
    task = Task.current_task(default_project_name=PROJECT_NAME, default_task_name=TASK_NAME)
    seed = task.get_random_seed()
    make_deterministic(seed)  # setup random seed from task for reproducibility

    #################################
    # Connect arguments to the task #
    #################################
    connect_to_task_parser = get_parser()
    task.connect(connect_to_task_parser)
    args = connect_to_task_parser.parse_args()
    if args.source_id == '':
        args.source_id = None

    logger = task.get_logger()
    logger.set_default_upload_destination(uri=args.upload_destination)
    logger.console('Running arguments: %s' % str(args))
    logger.flush()

    # Deprecation warning
    if hasattr(args, 'max_iterations'):
        logger.warn('"--max-iterations" is deprecated.')
        logger.warn('You should use "--max-train-iterations", which will be set to {}'.format(args.max_iterations))
        args.max_train_iterations = int(args.max_iterations)
    if hasattr(args, 'test_size'):
        logger.warn('"--test-size" is deprecated.')
        logger.warn('You should use "--max-test-iterations", which will be set to {}'.format(args.test_size))
        args.max_test_iterations = int(args.test_size)
    # TODO: update task parameters and remove deprecated ones using task.set_parameters()

    ##################################################
    # Defining input model connecting it to the task #
    ##################################################
    # Here we import a model from a url using import_model, since the user might not have a registered model yet
    # alternatively, if you already have a model id you could use this:
    # input_model = InputModel(model_id='12345')
    input_model_url, input_model_name = \
        get_model_url(args.feature_extraction_type, start_from_model_zoo=True)
    # Default labels for imported models that do not exist yet:
    input_label_enumeration =\
        {'hard': -2, 'ignore': -1, 'background': 0, 'aeroplane': 1, 'bicycle': 2, 'bird': 3, 'boat': 4, 'bottle': 5,
         'bus': 6, 'car': 7, 'cat': 8, 'chair': 9, 'cow': 10, 'diningtable': 11, 'dog': 12, 'horse': 13,
         'motorbike': 14, 'pottedplant': 15, 'person': 16, 'sheep': 17, 'sofa': 18, 'train': 19, 'tvmonitor': 20}

    input_model = InputModel.import_model(weights_url=input_model_url, name=input_model_name,
                                          design=None, label_enumeration=input_label_enumeration)
    task.connect(input_model)

    #########################
    # Defining output model #
    #########################
    output_model = OutputModel(task=task, framework="PyTorch")
    output_model.set_upload_destination(uri=args.upload_destination)

    if not output_model.design:
        logger.console('Loading default design into output model')
        default_design_file = Path('ssd_design.yaml')
        if not default_design_file.exists():
            raise IOError('Missing model design file in repo {}'.format(default_design_file))
        output_model.update_design(default_design_file.read_text())

    ######################################
    # Reading model design configuration #
    ######################################
    config_params = yaml.load(output_model.design)
    score_thresh = config_params.get('test_detection_conf_thresh', 0.5)
    try:
        target_image_size = ImageSizeTuple(w=config_params['input_dim_w'], h=config_params['input_dim_h'])
    except KeyError:
        # backwards compat
        target_image_size = ImageSizeTuple(w=config_params['min_dim_w'], h=config_params['min_dim_h'])

    #########################################################################################
    # Defining allegro's dataview to be used as the train set and connecting it to the task #
    #########################################################################################
    logger.console('Creating train dataview...')
    train_dataview = DataView(iteration_order=IterationOrder.random, iteration_infinite=True)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Car')
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Pedestrian', weight=2)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Person_sitting')
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Van', weight=2)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Cyclist', weight=2)
    train_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training', roi_query='Truck', weight=2)

    # Any custom augmentations used must be registered as custom augmentations
    ImageFrame.register_custom_augmentation(operation_name='AugmentationZoomInOut',
                                            augmentation_class=CustomAugmentationZoom)
    # add custom augmentation (Zoom In/Out)
    ImageFrame.register_custom_augmentation(operation_name='FixedCropSize',
                                            augmentation_class=CustomAugmentationFixedCropSizeWithProb)

    # Add Augmentation to train dataview
    aug_aff = Augmentation.Affine
    aug_pix = Augmentation.Pixel
    train_dataview.add_augmentation_custom(operations=['FixedCropSize'],
                                           arguments={'FixedCropSize': {'width': target_image_size[0],
                                                                        'height': target_image_size[1],
                                                                        'random_box_prob': 0.0
                                                                        }})
    train_dataview.add_augmentation_custom(operations=['AugmentationZoomInOut'], strength=1.5)
    train_dataview.add_augmentation_affine(strength=1.0,
                                           operations=(aug_aff.bypass, aug_aff.reflect_horizontal))
    train_dataview.add_augmentation_affine(strength=1.5,
                                           operations=(aug_aff.bypass, aug_aff.rotate, aug_aff.shear, aug_aff.scale))
    train_dataview.add_augmentation_pixel(strength=1,
                                          operations=(aug_pix.bypass, aug_pix.blur, aug_pix.noise))
    train_dataview.add_augmentation_pixel(strength=1.5,
                                          operations=(aug_pix.bypass, aug_pix.recolor))
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Person_sitting', to_label='Person')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Pedestrian', to_label='Person')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Cyclist', to_label='Person')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Van', to_label='Car')
    train_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                    from_labels='Truck', to_label='Car')

    task.connect(train_dataview)

    ##################################################
    # Defining task's labels (class name to integer) #
    ##################################################
    script_labels = {'hard': -2, 'ignore': -1, 'background': 0,  'Car': 1, 'Person': 2}
    train_dataview.set_labels(script_labels)
    output_model_labels = train_dataview.get_labels()
    labels_ok = check_labels(output_model_labels)
    output_model.update_labels(output_model_labels)

    #########################################
    # Adding auxiliary dataview as test set #
    #########################################
    test_dataview = DataView(iteration_order=IterationOrder.sequential, iteration_infinite=False)
    test_dataview.add_query(dataset_name='KITTI-2D-OBJECT', version_name='training')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='largely_occluded', to_label='hard')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Person_sitting', to_label='Person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Pedestrian', to_label='Person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Cyclist', to_label='Person')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Van', to_label='Car')
    test_dataview.add_mapping_rule(dataset_name='KITTI-2D-OBJECT', version_name='training',
                                   from_labels='Truck', to_label='Car')
    test_dataview.set_labels(output_model_labels)
    test_dataview_exists = task.connect_auxiliary('val', test_dataview)
    test_while_train = args.test_while_train
    if not test_dataview_exists:
        test_dataview = None
        logger.console('Auxiliary test dataview "val" was not found - deactivating test while train', level=ERROR)
        test_while_train = 0

    ######################################################
    # Apply mapping to the class that draws debug images #
    ######################################################
    id_to_label_mapping = {v: k for k, v in output_model_labels.items()}
    draw_boxes_with_mapping = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping, logger=logger)
    if test_while_train:
        label_to_id_mapping_for_test = test_dataview.get_labels()
        id_to_label_mapping_for_test = {v: k for k, v in label_to_id_mapping_for_test.items()}
        draw_boxes_with_mapping_for_test = DrawBoxesAndLabels(labels_mapping=id_to_label_mapping_for_test,
                                                              logger=logger, score_threshold=score_thresh)
    else:
        label_to_id_mapping_for_test = None
        draw_boxes_with_mapping_for_test = None

    #############################################################
    # Build the SSD net, adjusting for number of labels/classes #
    #############################################################
    # Adjust num-classes if there are more model labels than classes
    model_num_positive_classes = len([v for _, v in output_model_labels.items() if v > 0])
    design_num_classes = config_params.setdefault('num_classes', 1 + model_num_positive_classes)
    if design_num_classes != 1 + model_num_positive_classes:
        logger.console('Model design contained different number of classes than expected. Fixing to %d classes' % (
                    1 + model_num_positive_classes), WARNING)
        config_params['num_classes'] = 1 + model_num_positive_classes
        output_model.update_design(yaml.dump(config_params))
        logger.console('If you are purposely changing the number of classes, ignore this message', WARNING)
    else:
        logger.console('Now creating an SSD model for %d classes + bkg class' % model_num_positive_classes)

    ###########################################
    # Setting up Pytorch and building network #
    ###########################################
    logger.console('Setup pytorch...')
    cuda_on = setup_pytorch(args=args, logger=logger, suppress_warnings=True)
    if cuda_on:
        logger.console('Getting device count:')
        device_count = torch.cuda.device_count()
        msg_device = '... There are %d devices ...' % device_count if device_count > 1 else '... Found 1 device ...'
        logger.console(msg_device)

    logger.console('Constructing the SSD net...')
    size_threshold_for_300_variant = 400
    if min(target_image_size) >= size_threshold_for_300_variant:
        variant = 512
    else:
        variant = 300
        try:
            if len(config_params['max_sizes']) > 6:
                logger.warn('The given "max_sizes" list is too long for small images. Last value will be removed')
                config_params['max_sizes'] = config_params['max_sizes'][:6]
            if len(config_params['min_sizes']) > 6:
                logger.warn('The given "min_sizes" list is too long for small images. Last value will be removed')
                config_params['min_sizes'] = config_params['min_sizes'][:6]
            adaptive = NEW_ASPECT_RATIO_KEY in config_params.keys()
            key = 'aspect_ratios' if not adaptive else NEW_ASPECT_RATIO_KEY
            if len(config_params[key]) > 6:
                logger.warn('The given "{}" list is too long for small images. Third value will be removed'.format(key))
                config_params[key].pop(2)
            output_model.update_design(yaml.dump(config_params))
        except KeyError:
            logger.warn("ssd design is new format, cannot ascertain match of design to 300 variant.")

    if args.optimize_priors_sample_size > 0:
        logger.console('Optimizing priors for dataview, this may take a while')
        logger.console('(Downloaded image data is kept in the cache folder for the following training)')
        if 'prior_design' in config_params:
            logger.warn("prior_design already in input model design. Are you sure you wish to optimize priors again?")
        # noinspection PyArgumentList
        options = PriorOptimizationOptions(
            n_samples=args.optimize_priors_sample_size,
            max_n_clusters=7,
            cluster_threshold=0.5,
            target_size_w=target_image_size.w,
            target_size_h=target_image_size.h,
            equalize_dataview_queries=True if args.equalize_dataview_for_optimize_priors else False,
            resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO,
            scanning_mode=False  # don't need because crop is in effect
        )
        # changes are done in-place
        new_config_params = config_params.copy()
        new_config_params = optimize_priors(train_dataview, args, new_config_params, options=options, logger=logger)
        config_params = new_config_params

    # add accurate timing information
    if args.report_debug_info:
        timer_flag = 'cuda' if cuda_on else 'cpu'
        config_params['benchmark_timers'] = timer_flag

    ssd_net, freeze_n = build_ssd(args, config_params, variant, target_image_size, logger=logger)

    # create priors for ssd_net
    fake_image = torch.zeros([1, 3, target_image_size.h, target_image_size.w], dtype=torch.float32)
    ssd_net.generate_priors(feature_sizes_wh=ssd_net.get_multiscale_feature_sizes(input_image=fake_image))

    # we dont want to propagate benchmark to output model
    config_params.pop('benchmark_timers', None)
    output_model.update_design(yaml.dump(config_params))

    if not args.end2end_train:
        # we freeze the feature extractor
        logger.console('end2end not set - assuming TRANSFER LEARNING, freezing first %d layers' % freeze_n)
        freeze_first_n_layers(ssd_net.features, freeze_n)

    ###################################
    # Load input model weights to net #
    ###################################
    weights_file = input_model.get_weights()
    if weights_file is None:
        init_weight_function = partial(default_weight_init_for_module, method='xavier', logger=logger)
        logger.console('Initializing weights...')
        ssd_net.apply(init_weight_function)
    else:
        logger.console('Loading weights from: {}'.format(weights_file))
        # If you need to bootstrap from an unsupported model, you can supply a translation dict
        # where the keys are the names of the layers in the current model and the values are your layer names.
        loading_kwargs = dict()

        ssd_net.forgiving_load(weights_file, net_type=args.feature_extraction_type,
                               logger=logger, **loading_kwargs)

    if cuda_on:
        logger.console('setting up DataParallel model')

    # wrap to enable training on multiple GPUs
    net = torch.nn.DataParallel(ssd_net) if cuda_on else ssd_net

    #############################
    # Configure loss and solver #
    #############################

    logger.console('... Done. Now configuring optimizer')

    params = filter(lambda p: p.requires_grad, net.parameters())

    optimizer = torch.optim.SGD(params, lr=config_params['lr_list'][0],
                                momentum=args.momentum, weight_decay=args.weight_decay)

    lr_scheduler = CyclicLRScheduler(optimizer=optimizer,
                                     upper_bound_steps=config_params['lr_steps'],
                                     lr_list=config_params['lr_list'])

    # Deprecation warning
    update_needed = False
    if 'min_number_of_background_priors' not in config_params:
        logger.warn('DragonLoss has changed - model design requires "min_number_of_background_priors" parameter')
        config_params['min_number_of_background_priors'] = 100
        logger.warn('Default value of min_number_of_background_priors={} will be used'.format(
            config_params['min_number_of_background_priors']
        ))
        update_needed = True

    if 'hard_negative_mix_in_background' not in config_params:
        logger.warn('DragonLoss has changed - model design requires "hard_negative_mix_in_background" parameter')
        config_params['hard_negative_mix_in_background'] = 0.3
        logger.warn('Default value of hard_negative_mix_in_background={} will be used'.format(
            config_params['hard_negative_mix_in_background']))
        update_needed = True

    if 'dragon_loss_overlap_thresh' not in config_params:
        logger.warn('DragonLoss has changed - model design requires "dragon_loss_overlap_thresh" parameter')
        if 'multi_box_loss_overlap_thresh' in config_params:
            logger.warn('Converting from value of multi_box_loss_overlap_thresh={}'.format(
                config_params['multi_box_loss_overlap_thresh']
            ))
            config_params['dragon_loss_overlap_thresh'] = config_params.pop('multi_box_loss_overlap_thresh')
        else:
            config_params['dragon_loss_overlap_thresh'] = 0.4
            logger.warn('Default value of dragon_loss_overlap_thresh={} will be used'.format(
                config_params['dragon_loss_overlap_thresh']
            ))
        update_needed = True

    if 'dragon_small_roi_min_iou' not in config_params:
        logger.warn('DragonLoss has changed - model design requires "dragon_small_roi_min_iou" parameter')
        config_params['dragon_small_roi_min_iou'] = 0.2
        logger.warn('Default value of dragon_small_roi_min_iou={} will be used'.format(
            config_params['dragon_small_roi_min_iou']
        ))
        update_needed = True

    if update_needed:
        output_model.update_design(yaml.dump(config_params))

    dragon_match_options = dict(
        min_iou_for_small_box_match=config_params['dragon_small_roi_min_iou']
    )

    criterion = DragonLoss(num_classes=config_params['num_classes'],
                           overlap_thresh=config_params['dragon_loss_overlap_thresh'],
                           prior_for_matching=True,
                           bkg_label=config_params['background_label'],
                           neg_mining=True,
                           neg_pos=config_params['negative_positive_ratio'],
                           neg_overlap=0.5,
                           encode_target=False,
                           variance=config_params['variance'],
                           min_bkg=config_params['min_number_of_background_priors'],
                           hard_neg_mix=config_params['hard_negative_mix_in_background'],
                           use_gpu=cuda_on,
                           dragon_match=True,
                           dragon_match_kwargs=dragon_match_options)

    ##################
    # Start training #
    ##################
    logger.console('... Done. Here we go!')
    if test_while_train and args.test_before_train:
        task.mark_started()
        test_model(net, config_params, args, test_dataview,
                   test_mapping=label_to_id_mapping_for_test, test_draw_func=draw_boxes_with_mapping_for_test,
                   logger=logger, at_iteration=0, cuda_on=cuda_on)

    start_at = 0
    stop_at = args.max_train_iterations
    # Set the net to training mode when done testing
    net.train()
    train(start_at, stop_at)
