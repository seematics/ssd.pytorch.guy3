from copy import deepcopy
from random import Random
import numpy as np

from allegroai import ImageFrame
# from allegroai_api.services.v1_5.frames import Augmentation
from allegroai.debugging import get_null_logger
from allegroai_api.services import frames
from allegroai.utilities.augmentations import CustomAugmentationFixedCropLocation

from common import get_uri_from_frame_source, get_width_height_from_frame_source


def sliding_window_iterator_wrapper(iterator, crop_size_wh, overlap_percent, source_id, logger=None):

    logger = logger if logger else get_null_logger()

    warned_about_slow_image_loading = 0
    max_warn_about_slow_image_loading = 5

    crop_h = crop_size_wh[1]
    crop_w = crop_size_wh[0]
    crop_h_with_overlap = round(crop_h * overlap_percent)
    crop_w_with_overlap = round(crop_w * overlap_percent)

    try:
        ImageFrame.register_custom_augmentation(
            operation_name="FixedCropLocation",
            augmentation_class=CustomAugmentationFixedCropLocation,
        )
    except ValueError:
        # custom augmentation already registered
        pass

    def get_frame_width_height_slow(meta_frame, source_id):
        temp_metaframe = deepcopy(meta_frame)
        temp_metaframe.augmentation = []
        image_frame = ImageFrame(temp_metaframe)

        image = image_frame.get_data(source_id=source_id)
        if image is None:
            return -1, -1

        height, width, _ = image.shape

        return width, height

    def create_xy_patch_from_meta_frame(meta_frame, x, y):
        x = x if x >= 0 else 0
        y = y if y >= 0 else 0
        new_metaframe = deepcopy(meta_frame)
        curr_aug = frames.Augmentation(
            cls="custom",
            type="FixedCropLocation",
            strength=1.0,
            arguments={
                "x": x,
                "y": y,
                "width": crop_w,
                "height": crop_h,
            },
        )
        new_metaframe.augmentation.insert(0, curr_aug)
        new_metaframe.key_frame = meta_frame.id
        new_metaframe._upper_left_xy = [x, y]
        return new_metaframe

    try:
        for frame in iterator:

            frame_width, frame_height = get_width_height_from_frame_source(frame, source_id=source_id)

            if frame_height <= 0 or frame_width <= 0:
                # we must load the image in order to find out
                if warned_about_slow_image_loading <= max_warn_about_slow_image_loading:
                    if hasattr(logger, "warn"):
                        msg = """
                        ------------------------------------------------------------------------
                        SLOW OPERATION PERFORMED: (will warn you about it {n} times more)\n
                        frame does not contain height/width info - loading image to find out.\n
                        ------------------------------------------------------------------------
                        
                        """.format(n=max_warn_about_slow_image_loading-warned_about_slow_image_loading)
                        logger.warn(msg)
                    warned_about_slow_image_loading += 1
                frame_height, frame_width = get_frame_width_height_slow(frame, source_id=source_id)

                if frame_height < 0 or frame_width < 0:
                    if hasattr(logger, "warn"):
                        logger.warn("(Frame drop) Problem loading image {}".
                                    format(get_uri_from_frame_source(frame, source_id=source_id)))
                    continue

            upper_left_xy = [0, 0]
            j = 0
            while upper_left_xy[1] < frame_height:
                i = 0
                upper_left_xy[1] = j * crop_h - (j > 0) * crop_h_with_overlap

                if upper_left_xy[1] > (frame_height - crop_h):
                    upper_left_xy[1] = frame_height - crop_h

                while upper_left_xy[0] < frame_width:
                    upper_left_xy[0] = i * crop_w - (i > 0) * crop_w_with_overlap

                    if upper_left_xy[0] > (frame_width - crop_w):
                        upper_left_xy[0] = (frame_width - crop_w)

                    new_frame = create_xy_patch_from_meta_frame(frame, x=upper_left_xy[0], y=upper_left_xy[1])
                    if upper_left_xy[0] == (frame_width - crop_w):
                        yield new_frame
                        break
                    i += 1
                    yield new_frame
                if upper_left_xy[1] == (frame_height - crop_h):
                    break
                j += 1

    finally:
        if hasattr(iterator, "close"):
            iterator.close()


def custom_iterator_threshold_conf_level(an_iterator, thresh=0.0, drop_empty_frames=False):
    print('DEBUG - Custom Iterator Threshold Confidence Level: %.3f' % thresh)
    # use iterator in order to map
    for frame in an_iterator:
        # do the mapping
        frame.rois = [roi for roi in frame.rois if roi.confidence >= thresh]
        if drop_empty_frames and not frame.rois:
            continue
        yield frame


# mix between several iterators, resulting in an infinite iterator. clip length is used to match with iterators
# prepared with clip_iterator_from_images
def mix_iterators_infinite(list_iterators, list_probabilities, seed=1337, clip_length=31):
    assert len(list_iterators) == len(list_probabilities)
    left_over_frames = [None]*len(list_iterators)
    cumulative_prob = np.array(list_probabilities).cumsum()
    rng = Random(seed)
    while True:
        roll = rng.random() * cumulative_prob[-1]
        i = int(np.argmax(cumulative_prob > roll))
        selected_iterator = list_iterators[i]

        # continue from where we left
        if not left_over_frames[i]:
            left_over_frames[i] = next(selected_iterator)

        if not left_over_frames[i].key_frame:
            # no keyframe means it is not a video iterator - emulate clips
            for _ in range(clip_length):
                left_over_frames[i].key_frame = left_over_frames[i].id
                yield left_over_frames[i]
                left_over_frames[i] = next(selected_iterator)
        else:
            # get the entire clip (meaning until we switch the frame key.
            base_frame_key = left_over_frames[i].key_frame
            count = 0
            while left_over_frames[i].key_frame == base_frame_key:
                yield left_over_frames[i]
                count += 1
                left_over_frames[i] = next(selected_iterator)


# for "gifs", todo: make more efficient
def clip_iterator_from_images(finite_iterator, clip_length_odd=31, seed=1337, token_split_clips=None):
    """
    Clip iterator will generate clips (sequences) of length clip_length_odd from a finite iterator
    Note: the iterator turns a finite iterator to an infinite one
    :param finite_iterator: finite iterator, notice we first retrieve all the frames from the iterator
    :param clip_length_odd: clip length to get at the end (marked by frame.key_frame value)
    :param seed: seed number for the random choice of center key frame
    :param token_split_clips: split by src to dict and pick a clip from there
    """
    # Get all the frames here
    all_input_frames = [f for f in finite_iterator]
    all_input_frames.sort(key=lambda f: f.src)
    dict_clips = {}
    dict_clips_keys = []
    if token_split_clips is not None:
        for f in all_input_frames:
            k = f.src.split('/')[-token_split_clips]
            frame_list = dict_clips.get(k, [])
            frame_list.append(f)
            dict_clips[k] = frame_list
        dict_clips_keys = list(dict_clips.keys())
    clip_length_in_frames = 2 * (clip_length_odd // 2) + 1
    clip_radius = clip_length_in_frames // 2
    rnd = Random(seed)
    while True:
        if token_split_clips:
            # pick a clip
            clip_num = rnd.randint(0, len(dict_clips)-1)
            input_frames = dict_clips[dict_clips_keys[clip_num]]
        else:
            input_frames = all_input_frames
        key_frame_index = rnd.randint(clip_length_in_frames // 2,
                                      len(input_frames) - clip_length_in_frames // 2)
        # surround with clip_length
        base_frame = input_frames[key_frame_index]
        clip = input_frames[key_frame_index - clip_radius:key_frame_index + clip_radius + 1]
        for frame in clip:
            copy_f = deepcopy(frame)
            copy_f.key_frame = str(key_frame_index)
            copy_f.augmentation = base_frame.augmentation
            yield copy_f


# This is what we would like to do for video or clips with fake key_frames
def match_augmentations_to_key_frame_iterator(iterator):
    prev_key_frame = None
    base_augmentation = None
    for frame in iterator:
        if frame.key_frame:
            if frame.key_frame != prev_key_frame:
                prev_key_frame = frame.key_frame
                base_augmentation = frame.augmentation
            frame.augmentation = base_augmentation
        yield frame
