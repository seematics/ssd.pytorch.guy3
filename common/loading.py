import logging
import os
from pathlib import Path
from functools import partial

import numpy as np
from torch import nn, load
from torch.nn import init as init


def default_weight_init_for_module(m, method='xavier', logger=None):
    # note for SSD - no need to initalize L2Norm
    if isinstance(m, nn.Conv2d):
        if not method.startswith('xavier'):
            raise NotImplemented('only xavier style init is implemented')
        if logger:
            logger.console('Initalizing Conv2d with: %s' % m)
        if method == 'xavier_normal':
            init.xavier_normal_(m.weight.data)
        else:
            init.xavier_uniform_(m.weight.data)
        if m.bias is not None:
            m.bias.data.zero_()
    elif isinstance(m, nn.BatchNorm2d):
        m.weight.data.fill_(1)
        m.bias.data.zero_()


def xavier_init(torch_tensor, normal_instead_of_uniform=False):
    if normal_instead_of_uniform:
        init.xavier_normal_(torch_tensor)
    else:
        init.xavier_uniform_(torch_tensor)


class ModuleWithForgivingLoad(nn.Module):
    """
    handle loading of weights to a module if there are discrepancies between the state dicts
    limitation - module needs to have torchvision style naming convention at least for the feature extractor:

    """

    def __init__(self, logger=None):
        super().__init__()
        self._logger = logger

    @staticmethod
    def analyze_state_dict(state_dict):
        allegro_names = ['L2Norm', 'conf', 'extras', 'features', 'loc']
        children_names = []
        for name, param in state_dict.items():
            children_names.append(name.split('.')[0])
        children_names = np.unique(np.array(children_names))
        if all([name in allegro_names for name in children_names]):
            model_type = 'allegro'
        elif 'features' in children_names:
            model_type = 'allegro_like'
        else:
            model_type = 'other'
        return model_type, children_names

    @staticmethod
    def replace_name(name, net_type):
        new_name = name
        if 'resnet' in net_type:
            translator = {'conv1': 'features.0', 'bn1': 'features.1', 'layer1': 'features.4', 'layer2': 'features.5',
                          'layer3': 'features.6', 'layer4': 'features.7'}
            if name in translator.keys():
                new_name = translator[name]
        return new_name

    # TODO - smart key matching beyond just looking for the 'forgive_str' in the dict
    def forgiving_load(self, from_filename_str, net_type=None,
                       init_func_on_skip=init.xavier_uniform_, logger=None, **kwargs):
        try:
            filename = Path(from_filename_str)
        except TypeError:
            raise FileNotFoundError("Problem with weight file name. got: {}".format(from_filename_str))
        if not filename.exists():
            raise FileNotFoundError("Weights file {} could not be found".format(filename))
        if not ((filename.suffix == '.pkl') or (filename.suffix == '.pth')):
            raise IOError('Sorry, only .pth and .pkl files supported, model loading failed.')

        hack_name_translation = kwargs.pop('hack_name_translation', None)

        if logger:
            prnt_func = logger.console
        elif self._logger and hasattr(self._logger, 'console'):
            prnt_func = self._logger.console
        else:
            prnt_func = print

        # we begin with quiet init, so layers not loaded will still be initialized
        default_init = partial(default_weight_init_for_module, logger=None)
        self.apply(default_init)
        prnt_func('Loading weights into state dict...')
        incoming_state_dict = load(str(filename), map_location=lambda storage, loc: storage)
        own_state = self.state_dict()
        name_mapping = {name: None for name, _ in own_state.items()}
        warned_once = False
        incoming_type, incoming_children_names = self.analyze_state_dict(incoming_state_dict)
        for name, param in incoming_state_dict.items():
            name_parts = name.split('.')
            if incoming_type == 'other':
                name_parts[0] = self.replace_name(name=name_parts[0], net_type=net_type)
                revised_name = '.'.join(name_parts)
                if revised_name in own_state:
                    if not warned_once:
                        prnt_func('Seems like your input model is not an allegro SSD model. I Will try to mediate.')
                        warned_once = True
                    prnt_func('Incoming name {} will be renamed to {}'.format(name, revised_name))
                    name = revised_name
            # hack for mobilenet until torchvision model is available
            if incoming_type == 'allegro_like' and hack_name_translation is not None:
                if name in hack_name_translation.keys():
                    name = hack_name_translation[name]
            if name in own_state:
                if param.size() == own_state[name].size():
                    prnt_func('Match! %s : load_size: %s net_size: %s' % (
                        name, str(param.size()), str(own_state[name].size())))
                    own_state[name].copy_(param)
                    name_mapping[name] = str(param.size())
                elif init_func_on_skip is not None:
                    prnt_func('Cannot match %s because: load_size: %s net_size: %s' % (
                        name, str(param.size()), str(own_state[name].size())))
                    if name_parts[-1] in ('weight', 'bias'):
                        ModuleWithForgivingLoad.tensor_reconcile_load(
                            own_state[name], param,
                            init_func=init_func_on_skip,
                            reconcile_tile_if_possible=False,
                            logger=logger or self._logger,
                        )
                        name_mapping[name] = str(param.size())
                    else:
                        prnt_func('Now initializing using user supplied function')
                        init_func_on_skip(own_state[name])
                else:
                    prnt_func('*Skipping* %s: load_size: %s net_size: %s' % (
                        name, str(param.size()), str(own_state[name].size())))
            else:
                prnt_func('No match for layer {} in input model'.format(name))

        prnt_func('Finished loading weights')
        not_loaded = [name for name, val in name_mapping.items() if val is None]
        if not_loaded:
            prnt_func('Did not load weights for the following ssd layers (init only) - {}'.format(sorted(not_loaded)))

    @staticmethod
    def tensor_reconcile_load(target_param, input_param, init_func=init.xavier_uniform_,
                              reconcile_tile_if_possible=False, logger=None):
        """
        After our own reload_weights_from_similar_model in caffe.py
        because of pytorch 0.3 (or because I don't know pytorch well enough?) we
        infer the type of tensor to return by copying the input tensor.

        :param target_param: target weights
        :param input_param: source weights
        :param init_func: handler for irreconcilable parts
        :param reconcile_tile_if_possible: instead of using init_func, just tile the weights if possible
        :param logger: object that has .info, .warn method for printing
        :return: None
        """
        # TODO ... what is the alternative here?
        logger_info = print if logger is None else partial(logger.console, level=logging.INFO)
        logger_warn = print if logger is None else partial(logger.console, level=logging.WARN)

        target_size = target_param.size()
        input_size = input_param.size()
        logger_info('reconciling load from size %s to size %s' % (str(input_size), str(target_size)))
        # start by init
        if len(target_size) < 2:
            target_param.zero_()
        else:
            init_func(target_param)
        if len(target_size) != len(input_size):
            logger_warn('Actual tensor shapes do not match. Initializing empty weights')
            return
        # TODO - resize kernels if needed
        if np.any(input_size[2:] != target_size[2:]):
            raise NotImplemented('reconciling load does not work yet with different kernel size')
        else:
            copy_only = [min(target_size[i], input_size[i]) for i in range(len(input_size))]
            if len(input_size) == 1:
                target_param[:copy_only[0]] = input_param[:copy_only[0]]
            elif len(input_size) != 4:
                raise NotImplemented('only regular tensors implemented, not with dim = %d' % len(input_size))
            else:
                target_param[:copy_only[0], :copy_only[1], :, :] = \
                    input_param[:copy_only[0], :copy_only[1], :, :]
                if reconcile_tile_if_possible:
                    try:
                        if target_size[0] // 2 == input_size[0]:
                            target_param[copy_only[0]:, :, :, :] = \
                                input_param[:copy_only[0], :copy_only[1], :, :]
                        elif target_size[1] // 2 == input_size[1]:
                            target_param[:, copy_only[1]:, :, :] = \
                                input_param[:copy_only[0], :copy_only[1], :, :]
                        logger_info('Copied over the input weights twice as tiles!')
                    except ValueError as ex:
                        logger_info('Only the matching part from %s was copied, '
                                    'the rest was initialized' % str(input_size))
        return
