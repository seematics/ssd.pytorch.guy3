import functools
from collections import OrderedDict
from itertools import zip_longest
from pathlib import Path
from typing import Tuple

import numpy as np
import pandas
from allegroai import Dataset, DataView, Task, IterationOrder
from allegroai.debugging import get_logger
from allegroai.utilities.images import ResizeStrategy
from pathlib2 import Path
from sklearn.cluster import KMeans
from tqdm import tqdm

from common.prior_optimization import BoxAlignCalculator, AlignTypeEnum
from common.prior_optimization import PriorOptimizationOptions, KEEP_DATA, create_default_vis_options
from common.prior_optimization.bbox_clustering import get_all_bboxes_in_n_frames, emit_prior_wh_basis_from_net, \
    create_prior_df, create_match_scales_logic, create_new_cfg_with_priors, target_iou, filter_outliers, \
    get_min_max_object_scales_from_feature_size_array
from common.prior_optimization.bbox_clustering import index_to_prior_name_map
from common.utils import ImageSizeTuple

DEBUG = False
if DEBUG:
    from matplotlib import pyplot as plt
    plt.style.use('seaborn-talk')


@functools.lru_cache()
def get_dataview_size(dataview):
    """
    Calculates number of annotated frames in all versions configured in the given allegro's dataview,
    """
    input_datasets = dataview._dataview.data.versions
    view_size = 0
    for ds in input_datasets:
        dataset = Dataset.get(dataset_id=ds.dataset)
        ver = dataset.get_version(version_id=ds.version)
        view_size += sum(e.count for e in ver._version.data.stats.frames if e.name == 'annotated')

    if dataview._dataview.data.iteration.limit:
        return min(dataview._dataview.data.iteration.limit, view_size)
    else:
        return view_size


def get_dataview_md5hash(dataview):
    remove_fields = ['name', 'description']
    import hashlib
    import json
    dataview_obj = dataview._dataview.data.to_dict()
    for field in remove_fields:
        dataview_obj.pop(field)
    return hashlib.md5(json.dumps(dataview_obj, sort_keys=True).encode('utf-8')).hexdigest()


def _pick_clusters_based_on_weights(cluster_weights, k=0.5):
    """
    We are going to pick all the clusters that amount to >K percent of weight
    :param cluster_centers: (num_clusters, 2) center coordinates
    :param cluster_weights: (num_clusters, 1) weights, e.g. sum of some score for all the boxes in the cluster
    :return: IDXs of selected clusters
    """
    idxs = np.argsort(cluster_weights)
    above_threshold = (np.cumsum(cluster_weights[idxs]) / cluster_weights.sum()) >= k
    selected_clusters = idxs[above_threshold]
    return selected_clusters


def threshold_clusters(cluster_centers, cluster_weights, thresh=None, logger=None):
    # TODO - do something with the cluster weights
    if logger:
        logger.console('Thresholding clusters based on weights')
    new_cluster_idx = _pick_clusters_based_on_weights(cluster_weights, k=thresh)
    new_clusters = np.around(cluster_centers[new_cluster_idx, :])
    if logger:
        for i, cluster in enumerate(new_clusters):
            logger.console(' prior-{} : : [{} , {}]'.format(i, *cluster))
    return [cluster for cluster in new_clusters]


def try_equalize_queries(limited_dataview, logger=None):
    if logger:
        logger.warn("Equalizing queries is not supported yet! consider doing so manually")
    return limited_dataview


def optimize_priors(dataview: DataView, ssd_args, ssd_cfg, options: PriorOptimizationOptions = None, logger=None):

    logger = logger if logger else Task.current_task().get_logger()
    # noinspection PyArgumentList
    options = options if options else PriorOptimizationOptions(
        n_samples=2000,
        max_n_clusters=6,
        cluster_threshold=0.5,
        target_size_w=512,
        target_size_h=512,
        equalize_dataview_queries=False,
        resize_strategy=ResizeStrategy.BIGGER_KEEP_ASPECT_RATIO,
        scanning_mode=False
    )

    dv_size = get_dataview_size(dataview)

    if dv_size < options.n_samples:
        logger.warn('Input dataview is smaller than the requested sample size, adjusting to {}'.format(dv_size))
        options.n_samples = dv_size

    target_image_size = ImageSizeTuple(w=options.target_size_w, h=options.target_size_h)

    if options.scanning_mode:
        per_frame_options = dict(
            crop_width=target_image_size.w,
            crop_height=target_image_size.h,
        )
    elif all([size > 0 for size in target_image_size]):
        per_frame_options = dict(
            target_width_height=target_image_size,
            resize_strategy=options.resize_strategy
        )
    else:
        per_frame_options = None

    limited_dataview = dataview.clone()
    limited_params = dict(
        order=IterationOrder.random,
        infinite=False,
        maximum_number_of_frames=options.n_samples,
        random_seed=Task.current_task().get_random_seed()
    )
    limited_dataview.set_iteration_parameters(**limited_params)

    dataview = try_equalize_queries(limited_dataview) if options.equalize_dataview_queries else limited_dataview

    # for caching the gt_bbox result
    dataview_md5 = get_dataview_md5hash(dataview)

    pkl_name = "{md5}_{sample_size}_{target_w}x{target_h}_resize_{resize}.pkl".format(
        md5=dataview_md5,
        sample_size=options.n_samples,
        target_w=options.target_size_w,
        target_h=options.target_size_h,
        resize=options.resize_strategy
    )

    if Path(pkl_name).exists():
        if logger:
            logger.console("Found previous collection of rois from same dataview!")
        gt_bbox_df = pandas.read_pickle(pkl_name)
    else:
        gt_bbox_df = get_all_bboxes_in_n_frames(dataview=dataview,
                                                frame_kwargs=per_frame_options,
                                                n_samples=options.n_samples)

        if dataview.get_labels():
            bg_label = ssd_cfg.get('background_label', 0)
            if logger:
                logger.console("Dataview has label enumeration, removing all ROIs with id <= {}".format(bg_label))
            gt_bbox_df.drop(gt_bbox_df[gt_bbox_df.label <= bg_label].index, inplace=True)

        # if logger:
        #     logger.console("Filtering duplicates...")
        #
        # orig_len = len(gt_bbox_df)
        # gt_bbox_df.drop_duplicates(inplace=True)
        #
        # if orig_len > len(gt_bbox_df) and logger:
        #     logger.console("Dropped {} duplicates. Currently {} bboxes from {} requested frame samples".format(
        #         orig_len - len(gt_bbox_df), len(gt_bbox_df), options.n_samples
        #     ))

        gt_bbox_df.to_pickle(path=pkl_name)

    ssd_priors, match_group_info, fmaps = \
        emit_prior_wh_basis_from_net(ssd_args, ssd_cfg, target_image_size, logger=logger)

    priors_from_net_df = create_prior_df(ssd_priors, match_group_info)
    fmap_sizes = pandas.DataFrame(fmaps, columns=['width', 'height'])

    gt_bbox_df = filter_outliers(fmap_sizes, gt_bbox_df, drop_too_big=True, drop_too_small=True, logger=logger)

    low_bound = create_match_scales_logic(fmap_sizes, gt_bbox_df, logger=logger)

    min_object_scale, max_object_scale = get_min_max_object_scales_from_feature_size_array(fmap_sizes)

    clusters = []

    var1 = 'width'
    var2 = 'height'
    pvars = [var1, var2]

    if DEBUG:
        plt.gcf()
        plt.xlabel(var1)
        plt.ylabel(var2)
        plt.title('Dataview ROIs @ {im_w}x{im_h} [{var1} vs {var2}]'.format(
            im_w=target_image_size.w,
            im_h=target_image_size.h,
            var1=var1,
            var2=var2))

    for mg, match_group in enumerate(tqdm(low_bound, desc='using kmeans for finding bbox clusters per resolution')):
        # this_group = gt_bbox_df[match_group].drop_duplicates()
        this_group = gt_bbox_df[match_group].copy()
        points = this_group[pvars].values
        points = np.clip(points, a_min=0, a_max=max_object_scale)
        # # create kmeans object
        n_clusters = min(options.max_n_clusters, len(this_group))
        if len(this_group) < options.max_n_clusters and logger:
            logger.warn('Not enough samples in match group for n_clusters = {task}, reducing to {this}'.format(
                task=options.max_n_clusters, this=n_clusters))
        if n_clusters > 0:
            kmeans = KMeans(n_clusters=n_clusters, init='random', verbose=False)
            # # fitkmeans object to data
            kmeans.fit(points)
            # # print location of clusters learned by kmeans object
            y_km = kmeans.predict(points)
            this_group['major_cluster'] = y_km

            cluster_weights = this_group.groupby('major_cluster').sum()['overlap_score'].values

            logger.console('clustering result:')
            for i, cluster in enumerate(kmeans.cluster_centers_):
                logger.console(' prior_{} : (weight: {:4.2f}) : [{} , {}]'.format(
                    i, cluster_weights[i], *cluster.astype(np.int)))

            if DEBUG:
                # plot data according to match group
                color = 'C' + str(mg % 5 + 1)
                plt.scatter(this_group[var1].values,
                            this_group[var2].values, s=25, c=color)

            if DEBUG:
                # plot data according to cluster fit
                for i in reversed(range(n_clusters)):
                    plt.scatter(this_group[var1][y_km == i],
                                this_group[var2][y_km == i], s=35, marker='s', c=None, alpha=0.2)
                k_clusters = np.around(kmeans.cluster_centers_).astype(np.float)
                # plot also the original clusters
                ptr = plt.scatter(*k_clusters.T, s=250, c='yellow', marker='X', edgecolors='black', alpha=0.4)

            # merge centers
            new_clusters = threshold_clusters(kmeans.cluster_centers_, cluster_weights,
                                              thresh=options.cluster_threshold, logger=logger)
            #
            if DEBUG:
                # plot also the truncated clusters
                show_clusters = np.vstack(new_clusters)
                ptr = plt.scatter(*show_clusters.T, s=350, c='cyan', marker='X', edgecolors='black')

            clusters.append((mg, new_clusters))

        else:
            if logger:
                logger.warn("no cluster for match group!")
            clusters.append((mg, []))

    if DEBUG:
        plt.show()

    all_clusters = [cluster_group for _, cluster_group in clusters if len(cluster_group)]
    if not len(all_clusters):
        raise ValueError("Seems like clusters were found. Are you sure the dataview is OK?")
    all_clusters = np.vstack(all_clusters)
    match_groups = np.hstack([[mg] * len(cluster_group) for mg, cluster_group in clusters if len(cluster_group)])
    new_priors_guess_df = create_prior_df(all_clusters, match_groups)

    if logger:
        logger.console("Generating match report (intersecting priors with all bboxes sampled earlier...)")
        iou_thresh = ssd_cfg.get('dragon_loss_overlap_thresh', 0.4)
        generate_match_report(gt_bbox=gt_bbox_df, prev_priors=priors_from_net_df, new_priors=new_priors_guess_df,
                              label_dict=dataview.get_labels(), unique_str=dataview_md5,
                              match_iou=iou_thresh, logger=logger)

    new_cfg = create_new_cfg_with_priors(new_priors_guess_df, target_image_size, ssd_args, cfg=ssd_cfg, logger=logger)

    return new_cfg


def generate_match_report(gt_bbox: pandas.DataFrame, prev_priors: pandas.DataFrame, new_priors: pandas.DataFrame,
                          label_dict=None, match_iou=None, unique_str='', use_random_box_align=False, logger=None):

    use_random_box_align = use_random_box_align if match_iou and match_iou > 0 else False
    match_iou = match_iou if match_iou else 0.4
    logger = logger if logger else Task.current_task().get_logger()
    label_dict = {str(k): k for k in gt_bbox.label.unique()} if label_dict is None else label_dict

    # assert 'match_group' in prev_priors.columns, 'match_group information should be present in prev_priors'
    # assert 'match_group' in new_priors.columns, 'match_group information should be present in new_priors'

    align_strat = AlignTypeEnum.BottomLeft if not use_random_box_align else AlignTypeEnum.MultiRandom
    box_aligner = BoxAlignCalculator(align_strategy=align_strat)

    logger.console("Intersecting input priors with ground truth:")
    prev_stats = match_report_by_label(prev_priors, gt_bbox, match_iou, box_aligner=box_aligner,
                                       label_dict=label_dict, unique_str=unique_str, logger=logger)
    logger.console("Intersecting new priors with ground truth:")
    new_stats = match_report_by_label(new_priors, gt_bbox, match_iou, box_aligner=box_aligner,
                                      label_dict=label_dict, unique_str=unique_str, logger=logger)

    logger.console("Original prior match report:")
    logger.console('\n'+prev_stats.to_string())

    logger.console("Optimized prior match report:")
    logger.console('\n'+new_stats.to_string())


def match_report_by_label(priors_guess, gt_bbox, match_iou, box_aligner, label_dict, unique_str, logger):
    hashed = hash(priors_guess.to_csv())
    np_labels = gt_bbox.label.unique()
    if not np.any(np_labels > 0):
        np_labels = []
        raise RuntimeError('Expected at least one label that has positive id')
    new_mx_iou_w_prior, new_percent_matched_n = intersect_prior_with_gt_bbox_df(
        box_aligner=box_aligner,
        iou_min_threshold=match_iou,
        priors_df=priors_guess,
        gt_bbox_df=gt_bbox,
        column_name_map=index_to_prior_name_map('{}_iou_%s' % (str(match_iou)), priors_guess),
        precalc_score_df=None,
        precalc_score_name_map=None,
        vis_options=None,
        logger=logger,
        debug_result_data=Path('iou_pass_opt_dataview-{}_priors-{}_iou-{}.pkl'.format(unique_str, hashed, match_iou))
    )
    new_mx_iou_w_prior = new_mx_iou_w_prior.set_index(gt_bbox.index)
    new_stats = {}
    label_name_map = {_id: _name for _name, _id in label_dict.items()}
    for label in np_labels:
        label_group = gt_bbox.label == label
        label_name = label_name_map[label]
        new_stats[label_name] = new_mx_iou_w_prior[label_group].apply(lambda t: np.count_nonzero(t > match_iou), axis=0)
    match_stats = pandas.DataFrame(new_stats)
    return match_stats


def intersect_prior_with_gt_bbox_df(
        box_aligner: BoxAlignCalculator,
        iou_min_threshold: float,
        priors_df: pandas.DataFrame,
        gt_bbox_df: pandas.DataFrame,
        column_name_map: dict = None,
        precalc_score_df: pandas.DataFrame = None,
        precalc_score_name_map: dict = None,
        vis_options: dict = None,
        logger=None,
        override_pd_columns: list = None,
        debug_result_data: Path = None,
        no_match_fill_value=np.NaN,
) -> Tuple[pandas.DataFrame, pandas.Series]:
    logger = logger if logger else get_logger()

    def calc_percent_match_per_column(df: pandas.DataFrame) -> pandas.Series:
        return df.apply(lambda t: np.sum(~np.isnan(t)) / float(len(t)) * 100, axis=0)

    if debug_result_data and debug_result_data.exists():
        logger.console('using pre-calculated data in {}'.format(debug_result_data))
        result = pandas.read_pickle(path=str(debug_result_data))
        matched = calc_percent_match_per_column(result)
        return result, matched

    only_priors = list(column_name_map.keys()) if column_name_map else priors_df.index
    if precalc_score_name_map is not None:
        if column_name_map is not None:
            only_priors = {k: 'selected' for k in column_name_map if k in precalc_score_name_map.keys()}
        else:
            try:
                only_priors = only_priors.intersection(list(precalc_score_name_map.keys()))
            except Exception as ex:
                raise IndexError('without supplying column_name_map, precalc_score assumed the same index and failed')

    result = OrderedDict()
    vis_options = vis_options or create_default_vis_options()
    vis_option_flag = vis_options.get('viz_flag', False)
    min_iou_to_viz = vis_options.get('min_iou_to_viz', 0.5)
    col_query = ['width', 'height'] if not override_pd_columns else override_pd_columns

    if precalc_score_df is not None:
        assert type(precalc_score_df) is pandas.DataFrame, "sorry, no documentation yet"
        if precalc_score_name_map is None:
            logger.warn('Precalculated score dataframe without column name mapping, using column_name_map')
            precalc_score_name_map = column_name_map

    def create_query(gt_bbox: pandas.DataFrame, prior_name_in_df: str):
        precalc_col = []
        if precalc_score_df is not None:
            col_name = precalc_score_name_map[prior_name_in_df]
            precalc_col = precalc_score_df[col_name].values
        return zip_longest(gt_bbox[col_query].values, precalc_col)

    def prior_tqdm(priors: pandas.DataFrame, action_msg: str = None):
        msg = action_msg if action_msg else 'Intersecting'
        select = priors.index.isin(only_priors)
        return tqdm(zip(priors[select].index, priors[select][col_query].values),
                    total=len(only_priors), desc=msg + ' with priors...')

    def get_viz_flag_for_iter(score):
        if precalc is None:
            return True
        else:
            return precalc > min_iou_to_viz

    for prior_name, prior in prior_tqdm(priors_df):
        metric = []
        for bbox, precalc in create_query(gt_bbox_df, prior_name):
            if precalc and np.isnan(precalc):
                metric.append(no_match_fill_value)
                continue
            viz_flag = False if not vis_option_flag else get_viz_flag_for_iter(precalc)
            shifts = box_aligner(prior, bbox)
            score = np.max(target_iou(prior, bbox, shifts, vis_options=vis_options, visualize=viz_flag))
            score = score if score > iou_min_threshold else no_match_fill_value
            metric.append(score)
        column_name = column_name_map.get(prior_name) if column_name_map else prior_name
        result[column_name] = metric

    result = pandas.DataFrame(result)
    matched = calc_percent_match_per_column(result)

    if KEEP_DATA and debug_result_data:
        result.to_pickle(path=debug_result_data)

    return result, matched
